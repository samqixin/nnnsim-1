/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2014 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Zhu Li <phillipszhuli1990@gmail.com>
 *          Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 *   Original template made for ndnSIM for University of California,
 *   Los Angeles by Ilya Moiseenko
 */
#include <boost/ref.hpp>

#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/packet.h"
#include "ns3/pointer.h"
#include "ns3/simulator.h"
#include "ns3/uinteger.h"

#include "nnn-face.h"
#include "ns3/nnn-pdus.h"

#include "ns3/nnn-wire.h"
#include "ns3/wire-nnnsim-naming.h"

#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-forwarding-strategy.h"

#include "ns3/nnn-icn-pdus.h"

#include "ns3/nnn-icn-wire.h"
#include "ns3/wire-nnnsim-icn-naming.h"
#include "ns3/nnnsim-icn-data.h"
#include "ns3/nnnsim-icn-interest.h"

#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-icn-forwarding-strategy.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.Face");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (Face);

    TypeId
    Face::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::Face")
	      .SetParent<Object> ()
	      .SetGroupName ("Nnn")
	      .AddAttribute ("Id", "Face id (unique integer for the Nnn stack on this node)",
	                     TypeId::ATTR_GET, // allow only getting it.
	                     UintegerValue (0),
	                     MakeUintegerAccessor (&Face::m_id),
	                     MakeUintegerChecker<uint32_t> ())
	                     ;
      return tid;
    }

    /**
     * By default, 3N face are created in the "down" state
     *  with no MAC addresses.  Before becoming useable, the user must
     * invoke SetUp on them once an 3N address and mask have been set.
     */
    Face::Face (Ptr<Node> node)
    : m_node (node)
    , m_upstreamNULLpHandler (MakeNullCallback< void, Ptr<Face>, Ptr<NULLp> > ())
    , m_upstreamSOHandler    (MakeNullCallback< void, Ptr<Face>, Ptr<SO> > ())
    , m_upstreamLayerNULLpHandler (MakeNullCallback< void, Ptr<Face>, Ptr<NULLp>, const Address&, const Address&> ())
    , m_upstreamLayerSOHandler    (MakeNullCallback< void, Ptr<Face>, Ptr<SO>, const Address&, const Address&> ())
    , m_upstreamDOHandler    (MakeNullCallback< void, Ptr<Face>, Ptr<DO> > ())
    , m_upstreamDUHandler    (MakeNullCallback< void, Ptr<Face>, Ptr<DU> > ())
    , m_upstreamLayerDOHandler (MakeNullCallback< void, Ptr<Face>, Ptr<DO>, const Address&, const Address&> ())
    , m_upstreamLayerDUHandler (MakeNullCallback< void, Ptr<Face>, Ptr<DU>, const Address&, const Address&> ())
    , m_upstreamENHandler    (MakeNullCallback< void, Ptr<Face>, Ptr<EN> > ())
    , m_upstreamAENHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<AEN> > ())
    , m_upstreamRENHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<REN> > ())
    , m_upstreamDENHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<DEN> > ())
    , m_upstreamADENHandler  (MakeNullCallback< void, Ptr<Face>, Ptr<ADEN> > ())
    , m_upstreamOENHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<OEN> > ())
    , m_upstreamINFHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<INF> > ())
    , m_upstreamRHRHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<RHR> > ())
    , m_upstreamOHRHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<OHR> > ())
    , m_upstreamACKPHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<ACKP> > ())
    , m_upstreamLayerACKPHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<ACKP>, const Address&, const Address&> ())
    , m_upstreamInterestHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<Interest> > ())
    , m_upstreamDataHandler   (MakeNullCallback< void, Ptr<Face>, Ptr<Data> > ())
    , m_upstreamLayerInterestHandler (MakeNullCallback< void, Ptr<Face>, Ptr<Interest>, const Address&, const Address&> ())
    , m_upstreamLayerDataHandler (MakeNullCallback< void, Ptr<Face>, Ptr<Data>, const Address&, const Address&> ())
    , m_ifup (false)
    , m_num_poas (1)
    , m_id ((uint32_t)-1)
    , m_metric (5)
    , m_flags (0)
    , m_sharePoAData (false)
    , m_isPointToPoint (false)
    , m_isWiFi (false)
    {
      NS_LOG_FUNCTION (this << node);

      NS_ASSERT_MSG (node != 0, "node cannot be NULL. Check the code");

      EnableNminus3N (true);
      EnableNminusICN (true);
      EnableApp3N (true);
      EnableAppICN (true);
    }

    Face::~Face ()
    {
      NS_LOG_FUNCTION_NOARGS ();
    }

    Face::Face (const Face &)
    : m_ifup (false)
    , m_num_poas (1)
    , m_id ((uint32_t)-1)
    , m_metric (0)
    , m_flags (0)
    , m_sharePoAData (false)
    , m_isPointToPoint (false)
    , m_isWiFi (false)
    {
    }

    Face& Face::operator= (const Face &)
    {
      return *this;
    }

    Ptr<Node>
    Face::GetNode () const
    {
      return m_node;
    }

    void
    Face::RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw)
    {
      NS_LOG_FUNCTION_NOARGS ();

      if (fw->SupportsDirect3N ())
	{
	  NS_LOG_INFO ("Registering Face that supports direct 3N interaction");
	  m_upstreamNULLpHandler = MakeCallback (&ForwardingStrategy::OnNULLp, fw);
	  m_upstreamSOHandler = MakeCallback (&ForwardingStrategy::OnSO, fw);
	  m_upstreamDOHandler = MakeCallback (&ForwardingStrategy::OnDO, fw);
	  m_upstreamENHandler = MakeCallback (&ForwardingStrategy::OnEN, fw);
	  m_upstreamAENHandler = MakeCallback (&ForwardingStrategy::OnAEN, fw);
	  m_upstreamRENHandler = MakeCallback (&ForwardingStrategy::OnREN, fw);
	  m_upstreamDENHandler = MakeCallback (&ForwardingStrategy::OnDEN, fw);
	  m_upstreamADENHandler = MakeCallback (&ForwardingStrategy::OnADEN, fw);
	  m_upstreamINFHandler = MakeCallback (&ForwardingStrategy::OnINF, fw);
	  m_upstreamDUHandler = MakeCallback (&ForwardingStrategy::OnDU, fw);
	  m_upstreamOENHandler = MakeCallback (&ForwardingStrategy::OnOEN, fw);
	  m_upstreamRHRHandler = MakeCallback (&ForwardingStrategy::OnRHR, fw);
	  m_upstreamOHRHandler = MakeCallback (&ForwardingStrategy::OnOHR, fw);
	  m_upstreamACKPHandler = MakeCallback (&ForwardingStrategy::OnACKP, fw);

	  if (GetPoASharing())
	    {
	      m_upstreamLayerNULLpHandler= MakeCallback (&ForwardingStrategy::OnLayeredNULLp, fw);
	      m_upstreamLayerSOHandler = MakeCallback (&ForwardingStrategy::OnLayeredSO, fw);
	      m_upstreamLayerDOHandler = MakeCallback (&ForwardingStrategy::OnLayeredDO, fw);
	      m_upstreamLayerDUHandler = MakeCallback (&ForwardingStrategy::OnLayeredDU, fw);
	      m_upstreamLayerACKPHandler = MakeCallback (&ForwardingStrategy::OnLayeredACKP, fw);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Not propagating N-1 layer PoA information");
	    }
	}
      else
	{
	  NS_LOG_INFO ("Registering Face that doesn't support direct 3N interaction");
	}

      if (fw->SupportsDirectICN ())
	{
	  NS_LOG_INFO ("Registering Face that supports direct ICN interaction");
	  Ptr<ICN3NForwardingStrategy> icnfw = DynamicCast<ICN3NForwardingStrategy> (fw);
	  m_upstreamInterestHandler = MakeCallback (&ICN3NForwardingStrategy::OnInterest, icnfw);
	  m_upstreamDataHandler = MakeCallback (&ICN3NForwardingStrategy::OnData, icnfw);

	  if (GetPoASharing())
	    {
	      m_upstreamLayerInterestHandler = MakeCallback (&ICN3NForwardingStrategy::OnLayeredInterest, icnfw);
	      m_upstreamLayerDataHandler = MakeCallback (&ICN3NForwardingStrategy::OnLayeredData, icnfw);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Not propagating N-1 layer PoA information");
	    }
	}
      else
	{
	  NS_LOG_INFO ("Registering Face that doesn't support direct ICN interaction");
	}
    }

    void
    Face::UnRegisterProtocolHandlers ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      m_upstreamNULLpHandler = MakeNullCallback< void, Ptr<Face>, Ptr<NULLp> > ();
      m_upstreamSOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<SO> > ();
      m_upstreamDOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DO> > ();
      m_upstreamENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<EN> > ();
      m_upstreamAENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<AEN> > ();
      m_upstreamRENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<REN> > ();
      m_upstreamDENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DEN> > ();
      m_upstreamADENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<ADEN> > ();
      m_upstreamINFHandler = MakeNullCallback< void, Ptr<Face>, Ptr<INF> > ();
      m_upstreamDUHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DU> > ();
      m_upstreamOENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<OEN> > ();
      m_upstreamRHRHandler = MakeNullCallback< void, Ptr<Face>, Ptr<RHR> > ();
      m_upstreamOHRHandler = MakeNullCallback< void, Ptr<Face>, Ptr<OHR> > ();
      m_upstreamACKPHandler = MakeNullCallback< void, Ptr<Face>, Ptr<ACKP> > ();

      m_upstreamLayerNULLpHandler = MakeNullCallback< void, Ptr<Face>, Ptr<NULLp>, const Address&, const Address& > ();
      m_upstreamLayerSOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<SO>, const Address&, const Address& > ();
      m_upstreamLayerDOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DO>, const Address&, const Address& > ();
      m_upstreamLayerDUHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DU>, const Address&, const Address& > ();
      m_upstreamLayerACKPHandler = MakeNullCallback< void, Ptr<Face>, Ptr<ACKP>, const Address&, const Address& > ();

      m_upstreamInterestHandler = MakeNullCallback< void, Ptr<Face>, Ptr<Interest>> ();
      m_upstreamDataHandler = MakeNullCallback< void, Ptr<Face>, Ptr<Data>> ();

      m_upstreamLayerInterestHandler = MakeNullCallback< void, Ptr<Face>, Ptr<Interest>, const Address&, const Address&> ();
      m_upstreamLayerDataHandler = MakeNullCallback< void, Ptr<Face>, Ptr<Data>, const Address&, const Address&> ();
    }

    void
    Face::SetPoASharing (bool val)
    {
      m_sharePoAData = val;
    }

    bool
    Face::GetPoASharing ()
    {
      return m_sharePoAData;
    }

    void
    Face::SetPointToPoint (bool val)
    {
      m_isPointToPoint = val;
    }

    bool
    Face::IsPointToPoint ()
    {
      return m_isPointToPoint;
    }

    void
    Face::SetWiFi (bool val)
    {
      m_isWiFi = val;
    }

    bool
    Face::IsWiFi ()
    {
      return m_isWiFi;
    }


    bool
    Face::SendNULLp (Ptr<const NULLp> n_o)
    {
      NS_LOG_FUNCTION (this << n_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromNULLp (n_o));
    }

    bool
    Face::SendNULLp (Ptr<const NULLp> n_o, Address addr)
    {
      NS_LOG_FUNCTION (this << n_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromNULLp (n_o), addr);
    }

    bool
    Face::SendSO (Ptr<const SO> so_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << so_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromSO (so_o));
    }

    bool
    Face::SendSO (Ptr<const SO> so_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << so_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromSO (so_o), addr);
    }

    bool
    Face::SendDO (Ptr<const DO> do_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << do_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromDO (do_o));
    }

    bool
    Face::SendDO (Ptr<const DO> do_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << do_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromDO (do_o), addr);
    }

    bool
    Face::SendDU (Ptr<const DU> du_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << du_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromDU (du_o));
    }

    bool
    Face::SendDU (Ptr<const DU> du_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << du_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromDU (du_o), addr);
    }

    bool
    Face::SendEN (Ptr<const EN> en_o)
    {
      NS_LOG_FUNCTION (this << en_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromEN (en_o));
    }

    bool
    Face::SendEN (Ptr<const EN> en_o, Address addr)
    {
      NS_LOG_FUNCTION (this << en_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromEN (en_o), addr);
    }

    bool
    Face::SendAEN (Ptr<const AEN> aen_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << aen_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromAEN (aen_o));
    }

    bool
    Face::SendAEN (Ptr<const AEN> aen_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << aen_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromAEN (aen_o), addr);
    }

    bool
    Face::SendREN (Ptr<const REN> ren_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ren_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromREN (ren_o));
    }

    bool
    Face::SendREN (Ptr<const REN> ren_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ren_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromREN (ren_o), addr);
    }

    bool
    Face::SendDEN (Ptr<const DEN> den_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << den_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromDEN (den_o));
    }

    bool
    Face::SendDEN (Ptr<const DEN> den_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << den_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromDEN (den_o), addr);
    }

    bool
    Face::SendADEN (Ptr<const ADEN> aden_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << aden_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromADEN (aden_o));
    }

    bool
    Face::SendADEN (Ptr<const ADEN> aden_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << aden_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromADEN (aden_o), addr);
    }

    bool
    Face::SendOEN (Ptr<const OEN> oen_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << oen_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromOEN (oen_o));
    }

    bool
    Face::SendOEN (Ptr<const OEN> oen_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << oen_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromOEN (oen_o), addr);
    }

    bool
    Face::SendINF (Ptr<const INF> inf_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << inf_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromINF (inf_o));
    }

    bool
    Face::SendINF (Ptr<const INF> inf_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << inf_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromINF (inf_o), addr);
    }

    bool
    Face::SendRHR (Ptr<const RHR> rhr_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << rhr_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      return Send3N (Wire::FromRHR (rhr_o));
    }

    bool
    Face::SendRHR (Ptr<const RHR> rhr_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << rhr_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
   	{
   	  return false;
   	}

      InsertAddress(addr);

      return Send3N (Wire::FromRHR (rhr_o), addr);
    }

    bool
    Face::SendOHR (Ptr<const OHR> ohr_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ohr_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      return Send3N (Wire::FromOHR (ohr_o));
    }

    bool
    Face::SendOHR (Ptr<const OHR> ohr_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ohr_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      InsertAddress(addr);

      return Send3N (Wire::FromOHR (ohr_o), addr);
    }

    bool
    Face::SendInterest (Ptr<const Interest> interest_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << interest_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      return SendICN (icn::Wire::FromInterest (interest_o));
    }

    bool
    Face::SendInterest (Ptr<const Interest> interest_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << interest_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      InsertAddress(addr);

      return SendICN (icn::Wire::FromInterest (interest_o), addr);
    }

    bool
    Face::SendData (Ptr<const Data> data_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << data_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      return SendICN (icn::Wire::FromData (data_o));
    }

    bool
    Face::SendData (Ptr<const Data> data_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << data_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      InsertAddress(addr);

      return SendICN (icn::Wire::FromData (data_o), addr);
    }

    bool
    Face::Send3N (Ptr<Packet> packet)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      return true;
    }

    bool
    Face::Send3N (Ptr<Packet> packet, Address addr)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      InsertAddress(addr);

      return true;
    }

    bool
    Face::Receive3N (Ptr<const Packet> p)
    {
      NS_LOG_FUNCTION (this << p << p->GetSize ());

      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      Ptr<Packet> packet = p->Copy (); // give upper layers a rw copy of the packet
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::NULL_NNN:
	      return ReceiveNULLp (Wire::ToNULLp (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::SO_NNN:
	      return ReceiveSO (Wire::ToSO (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DO_NNN:
	      return ReceiveDO (Wire::ToDO (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::EN_NNN:
	      return ReceiveEN (Wire::ToEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::AEN_NNN:
	      return ReceiveAEN (Wire::ToAEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::REN_NNN:
	      return ReceiveREN (Wire::ToREN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::OEN_NNN:
	      return ReceiveOEN (Wire::ToOEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DEN_NNN:
	      return ReceiveDEN (Wire::ToDEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::ADEN_NNN:
	      return ReceiveADEN (Wire::ToADEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::INF_NNN:
	      return ReceiveINF (Wire::ToINF (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DU_NNN:
	      return ReceiveDU (Wire::ToDU (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::RHR_NNN:
	      return ReceiveRHR (Wire::ToRHR (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::OHR_NNN:
	      return ReceiveOHR (Wire::ToOHR (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::ACKP_NNN:
	      return ReceiveACKP (Wire::ToACKP (packet, Wire::WIRE_FORMAT_NNNSIM));
	    default:
	      NS_FATAL_ERROR ("Not supported 3N header");
	      return false;
	  }
	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown 3N header");
	  NS_FATAL_ERROR ("Unknown PDU type. Should not happen!");
	  return false;
      }

      return false;
    }

    bool
    Face::Receive3N (Ptr<const Packet> p, const Address &from, const Address &to)
    {
      NS_LOG_FUNCTION (this);

      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      InsertAddress(from);

      Ptr<Packet> packet = p->Copy (); // give upper layers a rw copy of the packet
      try
      {
	  // In general the only packets that need PoA sharing are NULLp and SO
	  // The rest are left unimplemented as the prior enrolling mechanism takes care
	  // of it all
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::NULL_NNN:
	      return ReceiveNULLp (Wire::ToNULLp (packet, Wire::WIRE_FORMAT_NNNSIM), from, to);
	    case nnn::SO_NNN:
	      return ReceiveSO (Wire::ToSO (packet, Wire::WIRE_FORMAT_NNNSIM), from, to);
	    case nnn::DO_NNN:
	      return ReceiveDO (Wire::ToDO (packet, Wire::WIRE_FORMAT_NNNSIM), from, to);
	    case nnn::EN_NNN:
	      return ReceiveEN (Wire::ToEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::AEN_NNN:
	      return ReceiveAEN (Wire::ToAEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::REN_NNN:
	      return ReceiveREN (Wire::ToREN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::OEN_NNN:
	      return ReceiveOEN (Wire::ToOEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DEN_NNN:
	      return ReceiveDEN (Wire::ToDEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::ADEN_NNN:
	      return ReceiveADEN (Wire::ToADEN (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::INF_NNN:
	      return ReceiveINF (Wire::ToINF (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DU_NNN:
	      return ReceiveDU (Wire::ToDU (packet, Wire::WIRE_FORMAT_NNNSIM), from, to);
	    case nnn::RHR_NNN:
	      return ReceiveRHR (Wire::ToRHR (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::OHR_NNN:
	      return ReceiveOHR (Wire::ToOHR (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::ACKP_NNN:
	      return ReceiveACKP (Wire::ToACKP (packet, Wire::WIRE_FORMAT_NNNSIM), from, to);
	    default:
	      NS_FATAL_ERROR ("Not supported 3N header");
	      return false;
	  }
	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown 3N header");
	  NS_FATAL_ERROR ("Unknown PDU type. Should not happen!");
	  return false;
      }

      return false;
    }

    bool
    Face::SendICN (Ptr<Packet> packet)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      return true;
    }

    bool
    Face::SendICN (Ptr<Packet> packet, Address addr)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminusICNEnabled ())
	{
	  return false;
	}

      return true;
    }

    bool
    Face::ReceiveICN (Ptr<const Packet> p)
    {
      NS_LOG_FUNCTION (this << p << p->GetSize ());

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      Ptr<Packet> packet = p->Copy (); // give upper layers a rw copy of the packet
      // Attempt to check ICN headers
      try
      {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(packet);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      return ReceiveInterest (icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM));
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      return ReceiveData (icn::Wire::ToData (packet, icn::Wire::WIRE_FORMAT_NDNSIM));
	      break;
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown ICN header.");
	  return false;
      }
      return false;
    }

    bool
    Face::ReceiveICN (Ptr<const Packet> p, const Address &from, const Address &to)
    {
      NS_LOG_FUNCTION (this << p << p->GetSize ());

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      InsertAddress(from);

      Ptr<Packet> packet = p->Copy (); // give upper layers a rw copy of the packet
      // Attempt to check ICN headers
      try
      {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(packet);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      return ReceiveInterest (icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM), from, to);
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      return ReceiveData (icn::Wire::ToData (packet, icn::Wire::WIRE_FORMAT_NDNSIM), from, to);
	      break;
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown ICN header.");
	  return false;
      }
      return false;
    }

    bool
    Face::SendACKP (Ptr<const ACKP> ackp_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ackp_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      return Send3N (Wire::FromACKP (ackp_o));
    }

    bool
    Face::SendACKP (Ptr<const ACKP> ackp_o, Address addr)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << ackp_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  return false;
	}

      InsertAddress(addr);

      return Send3N (Wire::FromACKP (ackp_o), addr);
    }

    bool
    Face::ReceiveDO (Ptr<DO> do_i, const Address& from, const Address& to)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      InsertAddress(from);

      m_upstreamLayerDOHandler (this, do_i, from, to);
      return true;
    }

    bool
    Face::ReceiveDU (Ptr<DU> du_i, const Address& from, const Address& to)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamLayerDUHandler (this, du_i, from, to);
      return true;
    }

    bool
    Face::ReceiveACKP (Ptr<ACKP> ack_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamACKPHandler (this, ack_i);
      return true;
    }

    bool
    Face::ReceiveACKP (Ptr<ACKP> ack_i, const Address& from, const Address& to)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamLayerACKPHandler (this, ack_i, from, to);
      return true;
    }

    bool
    Face::ReceiveNULLp (Ptr<NULLp> n_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
   	{
   	  return false;
   	}

      m_upstreamNULLpHandler (this, n_i);
      return true;
    }

    bool
    Face::ReceiveNULLp (Ptr<NULLp> n_i, const Address &from, const Address &to)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamLayerNULLpHandler (this, n_i, from, to);
      return true;
    }

    bool
    Face::ReceiveSO (Ptr<SO> so_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
   	{
   	  return false;
   	}

      m_upstreamSOHandler (this, so_i);
      return true;
    }

    bool
    Face::ReceiveSO (Ptr<SO> so_i, const Address &from, const Address &to)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamLayerSOHandler (this, so_i, from, to);
      return true;
    }

    bool
    Face::ReceiveDO (Ptr<DO> do_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsApp3NEnabled ())
   	{
   	  return false;
   	}

      m_upstreamDOHandler (this, do_i);
      return true;
    }

    bool
    Face::ReceiveEN (Ptr<EN> en_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamENHandler (this, en_i);
      return true;
    }

    bool
    Face::ReceiveAEN (Ptr<AEN> aen_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}


      m_upstreamAENHandler (this, aen_i);
      return true;
    }

    bool
    Face::ReceiveREN (Ptr<REN> ren_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}


      m_upstreamRENHandler (this, ren_i);
      return true;
    }

    bool
    Face::ReceiveDEN (Ptr<DEN> den_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamDENHandler (this, den_i);
      return true;
    }

    bool
    Face::ReceiveADEN (Ptr<ADEN> aden_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamADENHandler (this, aden_i);
      return true;
    }

    bool
    Face::ReceiveOEN (Ptr<OEN> oen_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamOENHandler (this, oen_i);
      return true;
    }

    bool
    Face::ReceiveINF (Ptr<INF> inf_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      m_upstreamINFHandler (this, inf_i);
      return true;
    }

    bool
    Face::ReceiveDU (Ptr<DU> du_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained DU src: (" << du_i->GetSrcName () << ") dst: (" << du_i->GetDstName () << ")");

      m_upstreamDUHandler (this, du_i);
      return true;
    }

    bool
    Face::ReceiveRHR (Ptr<RHR> rhr_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained RHR src: (" << rhr_i->GetSrcName () << ") dst: (" << rhr_i->GetDstName () << ")");

      m_upstreamRHRHandler (this, rhr_i);
      return true;
    }

    bool
    Face::ReceiveOHR (Ptr<OHR> ohr_i)
    {
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsApp3NEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained OHR src: (" << ohr_i->GetSrcName () << ") dst: (" << ohr_i->GetDstName () << ")");

      m_upstreamOHRHandler (this, ohr_i);
      return true;
    }

    bool
    Face::ReceiveInterest (Ptr<Interest> interest_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Interest");

      m_upstreamInterestHandler (this, interest_i);
      return true;
    }

    bool
    Face::ReceiveInterest (Ptr<Interest> interest_i, const Address &from, const Address &to)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Interest from " << from << " to " << to);

      m_upstreamLayerInterestHandler (this, interest_i, from, to);
      return true;
    }

    bool
    Face::ReceiveData (Ptr<Data> data_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Data");

      m_upstreamDataHandler (this, data_i);
      return true;
    }


    bool
    Face::ReceiveData (Ptr<Data> data_i, const Address &from, const Address &to)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsAppICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Data from " << from << " to " << to);

      m_upstreamLayerDataHandler (this, data_i, from, to);
      return true;
    }

    void
    Face::SetMetric (uint16_t metric)
    {
      NS_LOG_FUNCTION (metric);
      m_metric = metric;
    }

    uint16_t
    Face::GetMetric (void) const
    {
      return m_metric;
    }

    void
    Face::SetFlags (uint32_t flags)
    {
      m_flags = flags;
    }

    void
    Face::SetAddress (Address addr)
    {
      m_addr = Address (addr);
    }

    Address
    Face::GetAddress () const
    {
      return m_addr;
    }

    Address
    Face::GetBroadcastAddress () const
    {
      // Generally, Faces have no broadcast address
      return Address ();
    }

    uint16_t
    Face::GetBroadcastId ()
    {
      if (GetBroadcastAddress ().IsInvalid())
	{
	  return 0;
	}
      else
	{
	  return GetAddressId (GetBroadcastAddress ());
	}
    }

    uint16_t
    Face::GetAddressId (Address addr)
    {
      poa_num_bimap::left_iterator li = m_poas.left.find(addr);

      if (li != m_poas.left.end ())
	{
	  return li->second;
	}
      else
	{
	  // A 0 is an error
	  return 0;
	}
    }

    Address
    Face::GetIdAddress (uint16_t id)
    {
      poa_num_bimap::right_iterator ri = m_poas.right.find(id);

      if (ri != m_poas.right.end ())
	{
	  return ri->second;
	}
      else
	{
	  // A 0 is an error
	  return Address ();
	}
    }

    void
    Face::InsertAddress (Address addr)
    {
//      NS_LOG_FUNCTION (addr);
//      poa_num_bimap::left_const_iterator iter = m_poas.left.begin ();
//      NS_LOG_DEBUG ("Addresses before insert: ");
//      for ( ; iter != m_poas.left.end (); ++iter)
//	{
//	  NS_LOG_DEBUG ("" << iter->first << " --> " << iter->second);
//	}

      if (m_poas.left.find(addr) == m_poas.left.end ())
	{
	  NS_LOG_DEBUG ("Inserting " << addr << ", " << m_num_poas);
	  m_poas.insert(combo(addr, m_num_poas));
	  m_num_poas++;
	}

//      iter = m_poas.left.begin ();
//
//      NS_LOG_DEBUG ("Addresses after insert: ");
//           for ( ; iter != m_poas.left.end (); ++iter)
//     	{
//     	  NS_LOG_DEBUG ("" << iter->first << " --> " << iter->second);
//     	}
    }

    void
    Face::EnableNminus3N (bool enable)
    {
      m_nminus1_protocols["3N"] = enable;
    }

    bool
    Face::IsNminus3NEnabled ()
    {
      std::map<std::string,bool>::iterator it;
      it = m_nminus1_protocols.find ("3N");
      if (it != m_nminus1_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }

    void
    Face::EnableApp3N (bool enable)
    {
      m_n_protocols["3N"] = enable;
    }

    bool
    Face::IsApp3NEnabled ()
    {
      std::map<std::string,bool>::iterator it;
      it = m_n_protocols.find ("3N");
      if (it != m_n_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }

    void
    Face::EnableNminusICN (bool enable)
    {
      m_nminus1_protocols["ICN"] = enable;
    }

    bool
    Face::IsNminusICNEnabled ()
    {
      std::map<std::string,bool>::iterator it;
      it = m_nminus1_protocols.find ("ICN");
      if (it != m_nminus1_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }

    void
    Face::EnableAppICN (bool enable)
    {
      m_n_protocols["ICN"] = enable;
    }

    bool
    Face::IsAppICNEnabled ()
    {
      std::map<std::string,bool>::iterator it;
      it = m_n_protocols.find ("ICN");
      if (it != m_n_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }

    bool
    Face::IsNetworkCompatibilityEnabled (std::string str)
    {
      std::map<std::string,bool>::iterator it;
      it = m_nminus1_protocols.find (str);
      if (it != m_nminus1_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }

    bool
    Face::isAppFace() const
    {
      return (GetFlags () == Face::APPLICATION);
    }

    bool
    Face::operator== (const Face &face) const
    {
      NS_ASSERT_MSG (m_node->GetId () == face.m_node->GetId (),
                     "Faces of different nodes should not be compared to each other: " << *this << " == " << face);

      return (m_id == face.m_id);
    }

    bool
    Face::operator< (const Face &face) const
    {
      NS_ASSERT_MSG (m_node->GetId () == face.m_node->GetId (),
                     "Faces of different nodes should not be compared to each other: " << *this << " == " << face);

      return (m_id < face.m_id);
    }

    std::ostream&
    Face::Print (std::ostream &os) const
    {
      os << "id=" << GetId ();
      return os;
    }

    std::ostream&
    operator<< (std::ostream& os, const Face &face)
    {
      face.Print (os);
      return os;
    }

    bool
    Face::SendToApp (Ptr<Packet> p)
    {
      ConnectToApp(p);
      return false;
    }

    bool
    Face::SendToWire (Ptr<Packet> p, Address dst)
    {
      return Send3N (p, dst);
    }

    bool
    Face::ConnectToApp (Ptr<Packet> p)
    {
      NS_LOG_ERROR ("This should not be called!");
      return false;
    }
  } // namespace nnn
} // namespace ns3
