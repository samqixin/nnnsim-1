/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-producer-app.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-producer-app.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-producer-app.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef MODEL_APPS_NNN_ICN_PRODUCER_APP_H_
#define MODEL_APPS_NNN_ICN_PRODUCER_APP_H_

#include "ns3/random-variable-stream.h"

#include "ns3/nnn-icn-app.h"

namespace ns3
{
  namespace nnn
  {
    class ICNProducerApp : public ICNApp
    {
    public:
      static TypeId
      GetTypeId (void);

      ICNProducerApp ();

      virtual
      ~ICNProducerApp ();

      /**
       * @brief ICN related method to create a Data packet from a corresponding Interest
       */
      virtual Ptr<Packet>
      CreateReturnData (Ptr<Interest> interest);

      /**
       * @brief ICN MapMe related method to signal within ICN namespace that a producer is moving
       */
      virtual void
      SendMapMeInterest ();

    protected:
      virtual void
      StartApplication ();    // Called at time specified by Start

      virtual void
      StopApplication ();     // Called at time specified by Stop

      bool m_useMapMe;                       ///< @brief Flag to notify the use of MapMe Interest packets
      icn::Name m_prefix;                    ///< @brief The prefix for the Data created by this Producer
      uint32_t m_virtualPayloadSize;         ///< @brief Size of the virtual payload
      Time m_freshness;                      ///< @brief Freshness of Data packets

      uint32_t m_signature;                  ///< @brief Fake signature, 0 valid signature (default)
      icn::Name m_keyLocator;                ///< @brief Name to be used for key locator

      // MapMe related variables
      Time m_interestLifeTime;               ///< @brief LifeTime for Interest packet.
      uint32_t m_mapme_seq;                  ///< @brief Current sequence number used in MapMe Interest packets
      Ptr<UniformRandomVariable> uniRandom;  ///< @brief Uniform random number generator for Interest packet nonce creation
      Time m_mapme_retransmit;               ///< @brief Time for a MapMe Interest packet without an ACK to attempt to retransmit

      bool m_firstMapMe;
      uint32_t m_3n_mapme_flowid;
      uint32_t m_3n_mapme_seq;
      uint32_t m_3n_mapme_window;
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* MODEL_APPS_NNN_ICN_PRODUCER_APP_H_ */
