/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pure-icn-consumer-cbr.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pure-icn-consumer-cbr.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pure-icn-consumer-cbr.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_PURE_ICN_CONSUMER_CBR_H
#define NNN_PURE_ICN_CONSUMER_CBR_H

#include "nnn-pure-icn-consumer.h"

namespace ns3
{
  namespace nnn
  {
    class PureICNConsumerCbr : public PureICNConsumer
    {
    public:
      static TypeId GetTypeId ();

      PureICNConsumerCbr ();

      virtual
      ~PureICNConsumerCbr ();

    protected:
      virtual void
      ScheduleNextPacket ();

      void
      SetRandomize (const std::string &value);

      std::string
      GetRandomize () const;

    protected:
      double                         m_frequency; // Frequency of interest packets (in hertz)
      bool                           m_firstTime;
      bool                           useUni;
      Ptr<UniformRandomVariable>     uniRandom;
      Ptr<ExponentialRandomVariable> expRandom;
      std::string                    m_randomType;
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_PURE_ICN_CONSUMER_CBR_H */
