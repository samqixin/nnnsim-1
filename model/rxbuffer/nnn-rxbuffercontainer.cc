/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rxbuffercontainer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rxbuffercontainer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rxbuffercontainer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-rxbuffercontainer.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.rxbuffercontainer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (RxBufferContainer);

    TypeId
    RxBufferContainer::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::RxBufferContainer") // cheating ns3 object system
      	      .SetGroupName ("Nnn")
	      .SetParent<Object> ()
	      .AddConstructor<RxBufferContainer> ()
	;
      return tid;
    }

    RxBufferContainer::RxBufferContainer ()
    {
    }

    RxBufferContainer::~RxBufferContainer ()
    {
      m_buffer_container.clear ();
    }

    void
    RxBufferContainer::Remove (Ptr<Face> face)
    {
      NS_LOG_FUNCTION ("Removing Face: " << face->GetId ());

      FaceAddrContainer_by_face& face_index = m_buffer_container.get<FA_face> ();
      FaceAddrContainer_by_face::iterator it = face_index.begin ();

      for (; it != face_index.end ();)
	{
	  if (it->m_face_id == face->GetId())
	    it = face_index.erase(it);
	  else
	    ++it;
	}
    }

    void
    RxBufferContainer::Remove (Ptr<Face> face, Address dst)
    {
      NS_LOG_FUNCTION ("Removing Face: " << face->GetId () << " PoA: " << dst);

      FaceAddrContainer_by_composite& composite_index = m_buffer_container.get<FA_face_poa> ();
      FaceAddrContainer_by_composite::iterator it = composite_index.find (boost::make_tuple (face->GetId (), dst));

      if (it != composite_index.end ())
	{
	  composite_index.erase (it);
	}
      else
	{
	  NS_LOG_WARN ("Found no entry for Face: " << face->GetId () << " PoA: " << dst);
	}
    }

    std::set<Address>
    RxBufferContainer::GetPoADestinations (Ptr<Face> face)
    {
      NS_LOG_FUNCTION ("Get PoAs for Face " << face->GetId ());

      std::set<Address> ret;

      FaceAddrContainer_by_face& face_index = m_buffer_container.get<FA_face> ();
      FaceAddrContainer_by_face::iterator it = face_index.begin ();

      while (it != face_index.end ())
	{
	  if (it->m_face_id == face->GetId ())
	    {
	      ret.insert (it->m_dst_poa);
	    }
	  ++it;
	}

      return ret;
    }

    void
    RxBufferContainer::InsertPDU (Ptr<Face> face, Address dst, Ptr<DATAPDU> n_p, Time process)
    {
      NS_LOG_FUNCTION ("Insert Face: " << face->GetId () << " PoA: " << dst << n_p->GetFlowid () << n_p->GetSequence ());
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (!tmp)
	{
	  NS_LOG_WARN (" Buffer for Face: "  << face->GetId () << " PoA: " << dst << " returned null. Creating");
	  tmp = CreateBuffer (face, dst);
	}

      NS_LOG_DEBUG ("Before: " << *tmp);
      tmp->InsertPDU (n_p, process);
      NS_LOG_DEBUG ("After: " << *tmp);
    }

    void
    RxBufferContainer::AcknowledgePDU (Ptr<Face> face, Address dst, Ptr<ACKP> n_p)
    {
      NS_LOG_FUNCTION ("Acknowledging Face: " << face->GetId () << " PoA: " << dst);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (tmp)
	{
	  NS_LOG_DEBUG ("Before: " << *tmp);
	  tmp->AcknowledgePDU (n_p);
	  NS_LOG_DEBUG ("After: " << *tmp);
	}
      else
	{
	  if (dst != face->GetBroadcastAddress ())
	    {
	      NS_LOG_INFO ("Acknowledge wasn't found with PoA: " << dst << " attempting broadcast search");

	      tmp = FindBuffer (face, face->GetBroadcastAddress ());

	      if (tmp)
		{
		  NS_LOG_DEBUG ("Before: " << *tmp);
		  tmp->AcknowledgePDU (n_p);
		  NS_LOG_DEBUG ("After: " << *tmp);
		}
	      else
		{
		  NS_LOG_WARN ("Found no buffer for " << face->GetId () << " PoA: " << dst);
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Found no information for " << face->GetId () << " PoA: " << dst);
	    }
	}
    }

    bool
    RxBufferContainer::IsAcknowledged (Ptr<Face> face, Address dst,
				       uint32_t pdu_id, uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION ("Face: " << face->GetId () << " PoA: " << dst << " PDU: " << pdu_id << " Sequence: " << sequence);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (tmp)
	{
	  return tmp->IsAcknowledged (pdu_id, flowid, sequence);
	}
      else
	{
	  if (dst != face->GetBroadcastAddress ())
	    {
	      NS_LOG_INFO ("Acknowledge wasn't found with PoA: " << dst << " attempting broadcast search");

	      tmp = FindBuffer (face, face->GetBroadcastAddress ());

	      if (tmp)
		{
		  return tmp->IsAcknowledged (pdu_id, flowid, sequence);
		}
	      else
		{
		  NS_LOG_INFO ("There is no broadcast buffer for Face: " << face->GetId () << ", returning not acknowledged");
		  return false;
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("There is no buffer for Face: " << face->GetId () << " PoA: " << dst << ", returning not acknowledged. Verify code");
	      return false;
	    }
	}
    }

    bool
    RxBufferContainer::IsEmpty (Ptr<Face> face)
    {
      FaceAddrContainer_by_face& face_index = m_buffer_container.get<FA_face> ();
      FaceAddrContainer_by_face::iterator it = face_index.begin ();

      bool ret = true;

      while (it != face_index.end ())
	{
	  if (it->m_face_id == face->GetId ())
	    {
	      ret &= it->m_buffer->IsEmpty ();
	    }
	  ++it;
	}

      return ret;
    }

    Ptr<RxBuffer>
    RxBufferContainer::FindBuffer (Ptr<Face> face, Address dst)
    {
      FaceAddrContainer_by_composite& composite_index = m_buffer_container.get<FA_face_poa> ();
      FaceAddrContainer_by_composite::iterator it = composite_index.find (boost::make_tuple (face->GetId (), dst));

      if (it != composite_index.end ())
	{
	  return it->m_buffer;
	}
      else
	{
	  NS_LOG_INFO ("Found no record for Face: " << face->GetId () << " PoA: " << dst);
	  return 0;
	}
    }

    Ptr<RxBuffer>
    RxBufferContainer::CreateBuffer (Ptr<Face> face, Address dst)
    {
      FaceAddrContainer_by_composite& composite_index = m_buffer_container.get<FA_face_poa> ();
      FaceAddrContainer_by_composite::iterator it = composite_index.find (boost::make_tuple (face->GetId (), dst));

      if (it != composite_index.end ())
	{
	  return it->m_buffer;
	}
      else
	{
	  NS_LOG_DEBUG ("Creating buffer at request");
	  FaceAddr con = FaceAddr(face, dst);
	  m_buffer_container.insert(con);

	  return con.m_buffer;
	}
    }

    void
    RxBufferContainer::RemovePDU (Ptr<Face> face, Address dst, uint32_t pdu_id,
				  uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION ("Removing PDU from Face: " << face->GetId () << " PoA: " << dst);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (tmp)
	{
	  NS_LOG_DEBUG ("Before: " << *tmp);
	  tmp->RemovePDU (pdu_id, flowid, sequence);
	  NS_LOG_DEBUG ("After: " << *tmp);
	}
      else
	{
	  NS_LOG_WARN ("Found no information for Face: " << face->GetId () << " PoA: " << dst);
	}
    }

    void
    RxBufferContainer::Print (std::ostream &os) const
    {
      const FaceAddrContainer_by_composite& composite_index = m_buffer_container.get<FA_face_poa> ();
      FaceAddrContainer_by_composite::iterator it = composite_index.begin();

      for (; it != composite_index.end (); ++it)
	{
	  os << boost::format(rxbuffercontainer_formatter) % it->m_face_id %  it->m_dst_poa;
	  os << *(it->m_buffer);
	}
    }

    uint32_t
    RxBufferContainer::RemoveFlowid (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      NS_LOG_FUNCTION (face->GetId () << dst <<  flowid);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);
      uint32_t ret = 0;

      if (tmp)
	{
	  NS_LOG_DEBUG ("Before: " << *tmp);
	  ret = tmp->RemoveFlowid (flowid);
	  NS_LOG_DEBUG ("After: " << *tmp);
	  return ret;
	}
      else
	{
	  NS_LOG_WARN ("Found no information for Face: " << face->GetId () << " PoA: " << dst);
	  return ret;
	}
    }

    uint32_t
    RxBufferContainer::Size (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      NS_LOG_FUNCTION (face->GetId () << dst <<  flowid);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (tmp)
	{
	  return tmp->Size (flowid);
	}
      else
	{
	  NS_LOG_WARN ("Found no information for Face: " << face->GetId () << " PoA: " << dst);
	  return 0;
	}
    }

    void
    RxBufferContainer::RemovePDUExactFlowidSequence(Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION (face->GetId () << dst <<  flowid << sequence);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (tmp)
	{
	  NS_LOG_DEBUG ("Before: " << *tmp);
	  tmp->RemovePDUExactFlowidSequence (flowid, sequence);
	  NS_LOG_DEBUG ("After: " << *tmp);
	  return;
	}
      else
	{
	  NS_LOG_WARN ("Found no information for Face: " << face->GetId () << " PoA: " << dst);
	  return;
      	}
    }

    Ptr<DATAPDU>
    RxBufferContainer::RetrieveDATAPDU (Ptr<Face> face, Address dst,
				       uint32_t pdu_id, uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION ("Retrieve PDU from Face: " << face->GetId () << " PoA: " << dst);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (!tmp)
	{
	  return 0;
	}
      else
	{
	  return tmp->RetrieveDATAPDU (pdu_id, flowid, sequence);
	}
    }

    std::list<Ptr<DATAPDU> >
    RxBufferContainer::RetrieveDATAPDUByFlowid (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (!tmp)
	{
	  return std::list<Ptr<DATAPDU> > ();;
	}
      else
	{
	  NS_LOG_DEBUG ("Current: " << *tmp);
	  return tmp->RetrieveDATAPDUByFlowid (flowid);
	}
    }

    std::list<Ptr<DATAPDU> >
    RxBufferContainer::RetrieveDATAPDUByFlowidLessThanSeq (Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid << sequence);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (!tmp)
	{
	  return std::list<Ptr<DATAPDU> > ();;
	}
      else
	{
	  NS_LOG_DEBUG ("Current: " << *tmp);
	  return tmp->RetrieveDATAPDUByFlowidLessThanSeq (flowid, sequence);
	}
    }

    std::list<Ptr<DATAPDU> >
    RxBufferContainer::RetrieveDATAPDUByFlowidGreaterThanSeq (Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence, uint32_t window)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid << sequence << window);
      Ptr<RxBuffer> tmp = FindBuffer (face, dst);

      if (!tmp)
	{
	  return std::list<Ptr<DATAPDU> > ();;
	}
      else
	{
	  NS_LOG_DEBUG ("Current: " << *tmp);
	  return tmp->RetrieveDATAPDUByFlowidGreaterThanSeq (flowid, sequence, window);
	}
    }

  } /* namespace nnn */
} /* namespace ns3 */
