/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rxbuffer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rxbuffer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rxbuffer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-rxbuffer.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.rxbuffer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (RxBuffer);

    TypeId
    RxBuffer::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::RxBuffer")
	  .SetGroupName ("Nnn")
	  .AddConstructor<RxBuffer> ()
	  ;
      return tid;
    }

    RxBuffer::RxBuffer ()
    {
    }

    RxBuffer::~RxBuffer ()
    {
    }

    void
    RxBuffer::InsertPDU (Ptr<DATAPDU> n_p, Time process)
    {
      if (n_p)
	{
	  uint32_t pduid = n_p->GetPacketId();
	  uint32_t flowid = n_p->GetFlowid ();
	  uint32_t sequence = n_p->GetSequence ();

	  NS_LOG_DEBUG ("Attempting insertion of Flowid: " << flowid << " Sequence: " << sequence);

	  datapdu_buffer_by_seq& pdu_index = internal_pdu.get<seq_datapdu> ();
	  datapdu_buffer_by_seq::iterator itpdu = pdu_index.find (boost::make_tuple(pduid, flowid, sequence));

	  if (itpdu != pdu_index.end ())
	    {
	      NS_LOG_DEBUG ("Have found Flowid: " << flowid << " Sequence: " << sequence << ", updating process time to " << (Simulator::Now () + process));

	      // Have found this particular PDU, replace process time
	      DATAPDU_buffer_entry tmp = DATAPDU_buffer_entry (n_p, Simulator::Now () + process);

	      internal_pdu.replace(itpdu, tmp);
	    }
	  else
	    {
	      // Complete new PDU entry
	      NS_LOG_DEBUG ("Inserting Flowid: " << flowid << " Sequence: " << sequence << " process time to " << (Simulator::Now () + process));
	      internal_pdu.insert (DATAPDU_buffer_entry (n_p, Simulator::Now () + process));
	    }
	}
      else
	{
	  NS_LOG_WARN ("Given a null DATAPDU");
	  return;
	}
    }

    uint32_t
    RxBuffer::RemoveFlowid (uint32_t flowid)
    {
      NS_LOG_INFO ("Removing PDUs with Flowid: " << flowid);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);
      uint32_t ret = 0;

      while (it0 != it1)
	{
	  it0 = datapdu_index.erase (it0);
	  ret++;
	}

      return ret;
    }

    void
    RxBuffer::RemovePDU (uint32_t pdu_id, uint32_t flowid, uint32_t sequence)
    {
      datapdu_buffer_by_seq& seq_datapdu_index = internal_pdu.get<seq_datapdu> ();
      datapdu_buffer_by_seq::iterator itpdu = seq_datapdu_index.find(boost::make_tuple(pdu_id, flowid, sequence));

      if (itpdu != seq_datapdu_index.end ())
	{
	  seq_datapdu_index.erase (itpdu);
	}
    }

    void
    RxBuffer::RemovePDUByFlowidSequence (uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_INFO ("Removing PDUs with Flowid: " << flowid << " and sequence less than " << sequence);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  if (it0->GetSequence() <= sequence)
	    {
//	      NS_LOG_DEBUG ("Eliminating Flowid: " << flowid << " Sequence: " << it0->GetSequence ());
	      it0 = datapdu_index.erase (it0);
	    }
	  else
	    {
	      ++it0;
	    }
	}
    }

    void
    RxBuffer::RemovePDUExactFlowidSequence (uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_INFO ("Removing PDU with Flowid " << flowid << " and sequence " << sequence);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  if (it0->GetSequence() == sequence)
	    {
	      it0 = datapdu_index.erase (it0);
	    }
	  else
	    {
	      ++it0;
	    }
	}
    }

    void
    RxBuffer::AcknowledgePDU (Ptr<ACKP> n_p)
    {
      if (n_p)
	{
	  std::map<uint32_t, uint32_t> tmp = n_p->GetACKFlowids ();
	  std::map<uint32_t, uint32_t>::iterator it = tmp.begin ();

	  while (it != tmp.end ())
	    {
	      RemovePDUByFlowidSequence (it->first, it->second);
	      ++it;
	    }
	}
      else
	{
	  NS_LOG_WARN ("Received a null ACKP, check code");
	}
    }

    bool
    RxBuffer::IsAcknowledged (uint32_t pdu_id, uint32_t flowid, uint32_t sequence)
    {
      return (RetrieveDATAPDU (pdu_id, flowid, sequence) == 0);
    }

    Ptr<DATAPDU>
    RxBuffer::RetrieveDATAPDU (uint32_t pdu_id, uint32_t flowid, uint32_t sequence)
    {
      datapdu_buffer_by_seq& seq_datapdu_index = internal_pdu.get<seq_datapdu> ();
      datapdu_buffer_by_seq::iterator itpdu = seq_datapdu_index.find(boost::make_tuple(pdu_id, flowid, sequence));

      if (itpdu != seq_datapdu_index.end ())
	{
	  return itpdu->m_pdu->Copy ();
	}
      else
	{
	  return 0;
	}
    }

    bool
    RxBuffer::IsEmpty ()
    {
      return (internal_pdu.size () == 0);
    }

    void
    RxBuffer::Print (std::ostream &os) const
    {
      size_t pdu_size = internal_pdu.size ();

      bool printpdu = (pdu_size != 0);

      if (!printpdu)
	{
	  os << "All retransmission buffers empty" << std::endl;
	  return;
	}

      if (printpdu)
	{
	  const datapdu_buffer_by_seq& datapdu_index = internal_pdu.get<seq_datapdu> ();
	  datapdu_buffer_by_seq::iterator itpdu = datapdu_index.begin ();

	  os << " DATAPDU RxBuffer: " << pdu_size << std::endl;
	  os << "-------------------------------------------------------------------------------" << std::endl;
	  for (; itpdu != datapdu_index.end (); ++itpdu)
	    {
	      os << boost::format(rxbuffer_formatter) % itpdu->GetPDUId () % itpdu->GetFlowid () % itpdu->GetSequence () % itpdu->GetSrcName () % itpdu->GetDstName () % itpdu->GetProcessTime ().GetSeconds ();
	    }
	  os << "-------------------------------------------------------------------------------" << std::endl;
	}
    }

    std::list<Ptr<DATAPDU> >
    RxBuffer::RetrieveDATAPDUByFlowidLessThanSeq (uint32_t flowid, uint32_t sequence)
    {
      std::list<Ptr<DATAPDU> > retpdu;

      NS_LOG_INFO ("Retrieving PDUs with a Flowid: " << flowid << " and sequence smaller than " << sequence);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  if (it0->GetSequence() < sequence)
	    {
	      retpdu.push_back (it0->m_pdu->Copy ());
	    }

	  ++it0;
	}

      NS_LOG_DEBUG ("Have retrieved " << retpdu.size () << " PDUs");
      return retpdu;
    }

    std::list<Ptr<DATAPDU> >
    RxBuffer::RetrieveDATAPDUByFlowidGreaterThanSeq (uint32_t flowid, uint32_t sequence, uint32_t window)
    {
      std::list<Ptr<DATAPDU> > retpdu;
      uint32_t count = 0;

      NS_LOG_INFO ("Retrieving PDUs with a Flowid " << flowid << " and sequence greater than " << sequence);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while ((count < window) && (it0 != it1))
	{
	  if (sequence < it0->GetSequence())
	    {
	      retpdu.push_back (it0->m_pdu->Copy ());
	      count++;
	    }

	  ++it0;
	}

      NS_LOG_DEBUG ("Have retrieved " << retpdu.size () << " PDUs");
      return retpdu;
    }


    std::list<Ptr<DATAPDU> >
    RxBuffer::RetrieveDATAPDUByFlowid (uint32_t flowid)
    {
      std::list<Ptr<DATAPDU> > retpdu;

      NS_LOG_INFO ("Retrieving PDUs with a Flowid " << flowid);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  retpdu.push_back (it0->m_pdu->Copy ());

	  ++it0;
	}

      NS_LOG_DEBUG ("Have retrieved " << retpdu.size () << " PDUs");
      return retpdu;
    }

    std::list<Ptr<DATAPDU> >
    RxBuffer::FlushDATAPDUByFlowid (uint32_t flowid)
    {
      std::list<Ptr<DATAPDU> > retpdu;

      NS_LOG_INFO ("Flushing PDUs with a Flowid " << flowid);
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  retpdu.push_back (it0->m_pdu->Copy ());


	  it0 = datapdu_index.erase(it0);
	}

      NS_LOG_DEBUG ("Have retrieved " << retpdu.size () << " PDUs");
      return retpdu;
    }

    uint32_t
    RxBuffer::Size (uint32_t flowid)
    {
      uint32_t ret = 0;
      datapdu_buffer_by_flowid& datapdu_index = internal_pdu.get<flowid_datapdu> ();
      datapdu_buffer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = datapdu_index.equal_range (flowid);

      while (it0 != it1)
	{
	  ret++;
	  ++it0;
	}

      return ret;
    }

  } /* namespace nnn */
} /* namespace ns3 */
