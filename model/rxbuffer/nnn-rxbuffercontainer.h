/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rxbuffercontainer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rxbuffercontainer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rxbuffercontainer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_RXBUFFERCONTAINER_H_
#define NNN_RXBUFFERCONTAINER_H_

#include "ns3/object.h"
#include "ns3/log.h"

#include "nnn-rxbuffer.h"

#include "ns3/nnn-pdus.h"

#include <boost/format.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

namespace ns3
{
  namespace nnn
  {

    const boost::format rxbuffercontainer_formatter("%-40s|%-39s\n");

    class FaceAddr
    {
    public:

      FaceAddr (Ptr<Face> face, Address dst)
      : m_face_id (face->GetId ())
      , m_dst_poa (dst)
      , m_buffer  (Create<RxBuffer> ())
      {
      }

      virtual
      ~FaceAddr ()
      {
	m_buffer = 0;
      }

      bool
      operator < (const FaceAddr e) const
      {
	if (m_face_id == e.m_face_id)
	  {
	    return (m_dst_poa < e.m_dst_poa);
	  }
	else
	  {
	    return (m_face_id < e.m_face_id);
	  }
      }

      uint32_t      m_face_id; // ID of the Face used
      Address       m_dst_poa; // DST Address used
      Ptr<RxBuffer> m_buffer;  // Pointer to the RxBuffer associated to the Face/Address pair
    };

    struct FA_face_poa{};
    struct FA_face{};
    struct FA_poa{};

    typedef multi_index_container<
	FaceAddr,
	indexed_by<
	    ordered_unique<
	        tag<FA_face_poa>,
		composite_key<
		    FaceAddr,
		    member<FaceAddr, uint32_t, &FaceAddr::m_face_id>,
		    member<FaceAddr, Address, &FaceAddr::m_dst_poa>
                >
            >,

	    ordered_non_unique<
	        tag<FA_face>,
		member<FaceAddr, uint32_t, &FaceAddr::m_face_id>
            >,

	    ordered_non_unique<
	        tag<FA_poa>,
		member<FaceAddr, Address, &FaceAddr::m_dst_poa>
            >
         >
     > FaceAddrContainer;

    typedef FaceAddrContainer::index<FA_face_poa>::type FaceAddrContainer_by_composite;
    typedef FaceAddrContainer::index<FA_face>::type FaceAddrContainer_by_face;
    typedef FaceAddrContainer::index<FA_poa>::type FaceAddrContainer_by_poa;

    class RxBufferContainer : public Object
    {
    public:

      static TypeId GetTypeId ();

      RxBufferContainer ();

      virtual
      ~RxBufferContainer ();

      void
      Remove (Ptr<Face> face);

      void
      Remove (Ptr<Face> face, Address dst);

      std::set<Address>
      GetPoADestinations (Ptr<Face> face);

      void
      InsertPDU (Ptr<Face> face, Address dst, Ptr<DATAPDU> n_p, Time process);

      void
      AcknowledgePDU (Ptr<Face> face, Address dst, Ptr<ACKP> n_p);

      void
      RemovePDU (Ptr<Face> face, Address dst, uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      bool
      IsAcknowledged (Ptr<Face> face, Address dst, uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      uint32_t
      Size (Ptr<Face> face, Address dst, uint32_t flowid);

      bool
      IsEmpty (Ptr<Face> face);

      uint32_t
      RemoveFlowid (Ptr<Face> face, Address dst, uint32_t flowid);

      void
      RemovePDUExactFlowidSequence(Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence);

      Ptr<DATAPDU>
      RetrieveDATAPDU (Ptr<Face> face, Address dst, uint32_t pdu_id, uint32_t flowid, uint32_t sequence);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowid (Ptr<Face> face, Address dst, uint32_t flowid);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowidLessThanSeq (Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence);

      std::list<Ptr<DATAPDU> >
      RetrieveDATAPDUByFlowidGreaterThanSeq(Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence, uint32_t window);

      void
      Print (std::ostream &os) const;

    private:

      Ptr<RxBuffer>
      FindBuffer (Ptr<Face> face, Address dst);

      Ptr<RxBuffer>
      CreateBuffer (Ptr<Face> face, Address dst);

      FaceAddrContainer m_buffer_container;
    };

    inline std::ostream &
    operator << (std::ostream &os, const RxBufferContainer &rxbuffercon)
    {
      os << "RxBufferContainer:" << std::endl;
      os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
      os << boost::format(rxbuffercontainer_formatter) % "Face" % "PoA" ;
      os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
      rxbuffercon.Print (os);
      os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
      return os;
    }

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_RXBUFFERCONTAINER_H_ */
