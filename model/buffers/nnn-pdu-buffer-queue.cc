/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pdu-buffer-queue.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pdu-buffer-queue.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pdu-buffer-queue.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include <algorithm>

#include "nnn-pdu-buffer-queue.h"

#include "ns3/nnn-wire.h"
#include "ns3/wire-nnnsim-naming.h"
#include "ns3/nnn-icn-wire.h"

namespace ns3
{
  namespace nnn
  {
    PDUQueue::PDUQueue ()
    : m_lease_expire (Seconds (0))
    {
    }

    PDUQueue::PDUQueue (Time lease_expire)
    : m_lease_expire (lease_expire)
    {
    }

    PDUQueue::~PDUQueue ()
    {
    }

    bool
    PDUQueue::isEmpty()
    {
      return buffer.empty();
    }

    void
    PDUQueue::clear ()
    {
      std::queue<std::pair<Time, Ptr<Packet> > > empty;

      std::swap(buffer, empty);
    }

    Ptr<Packet>
    PDUQueue::pop ()
    {
      Ptr<Packet> tmp = buffer.front ().second;
      buffer.pop();
      return tmp;
    }

    void
    PDUQueue::push (Ptr<Packet> pdu, Time retx)
    {
      buffer.push (std::make_pair((Simulator::Now () + retx), pdu));
    }

    void
    PDUQueue::pushSO (Ptr<const SO> so_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromSO(so_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushDO (Ptr<const DO> do_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromDO(do_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushDU (Ptr<const DU> du_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromDU(du_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushNULLp (Ptr<const NULLp> nullp_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromNULLp(nullp_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushEN (Ptr<const EN> en_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromEN(en_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushAEN (Ptr<const AEN> aen_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromAEN(aen_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushOEN (Ptr<const OEN> oen_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromOEN(oen_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushREN (Ptr<const REN> ren_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromREN(ren_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushDEN (Ptr<const DEN> den_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromDEN(den_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushADEN (Ptr<const ADEN> aden_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromADEN(aden_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushINF (Ptr<const INF> inf_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromINF(inf_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushRHR (Ptr<const RHR> rhr_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromRHR(rhr_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushOHR (Ptr<const OHR> ohr_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromOHR(ohr_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushACKP (Ptr<const ACKP> ack_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), Wire::FromACKP(ack_p, Wire::WIRE_FORMAT_NNNSIM)));
    }

    void
    PDUQueue::pushData (Ptr<const Data> data_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), icn::Wire::FromData(data_p, icn::Wire::WIRE_FORMAT_DEFAULT)));
    }

    void
    PDUQueue::pushInterest (Ptr<const Interest> interest_p, Time retx)
    {
      buffer.push(std::make_pair((Simulator::Now () + retx), icn::Wire::FromInterest(interest_p, icn::Wire::WIRE_FORMAT_DEFAULT)));
    }

    std::queue<std::pair<Time, Ptr<Packet> > >
    PDUQueue::popQueue ()
    {
      return buffer;
    }

    uint
    PDUQueue::size ()
    {
      return buffer.size();
    }

    void
    PDUQueue::SetLeaseExpire (Time lease_expire)
    {
      m_lease_expire = lease_expire;
    }

    Time
    PDUQueue::GetLeaseExpire ()
    {
      return m_lease_expire;
    }
  } /* namespace nnn */
} /* namespace ns3 */
