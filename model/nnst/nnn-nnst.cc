/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2014 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-nnst.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-nnst.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-nnst.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/core.hpp>
#include <boost/ref.hpp>

#include "ns3/type-id.h"
#include "ns3/trace-source-accessor.h"

namespace ll = boost::lambda;

#include "nnn-nnst.h"
#include "ns3/nnn-nnst-entry.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.nnst");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (NNST);

    TypeId
    NNST::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::NNST") // cheating ns3 object system
	  .SetGroupName ("Nnn")
	  .SetParent<Object> ()
	  .AddTraceSource ("NameExpire", "Traces when a 3N name lease expires",
			   MakeTraceSourceAccessor (&NNST::m_name_expired),
			   "ns3::nnn::NNST::NNNAddressTracedCallback")
	  .AddConstructor<NNST> ()
	  ;
      return tid;
    }

    NNST::NNST() {
    }

    NNST::~NNST() {
    }

    Ptr<nnst::Entry>
    NNST::ClosestSector (const NNNAddress &prefix)
    {
//      NS_LOG_FUNCTION (this << prefix);

//      NS_LOG_DEBUG ("Running trie search");
      super::iterator item = super::longest_prefix_match (prefix);

      if (item == super::end ())
	{
//	  NS_LOG_DEBUG ("There seems to be no closest sector for " << prefix << " looking for anything close");
	  // We don't have a longest prefix with with the given address, attempt to order by distance
	  Ptr<nnst::Entry> curr;
	  Ptr<nnst::Entry> closest = Begin ();
	  for (curr = Begin(); curr != End (); curr = Next(curr))
	    {
	      if (curr->GetAddressPtr ()->distance(prefix) < closest->GetAddressPtr ()->distance (prefix))
		{
//                  NS_LOG_INFO ("Found " << *curr->GetAddressPtr () << " to be closer to " << prefix << "");
		  closest = curr;
		}
	    }
	  // Guard the logging, can return null
	  if (closest == 0)
	    {
//	      NS_LOG_DEBUG ("The search truly returned nothing for " << prefix);
	    }
	  else
	    {
//	      NS_LOG_DEBUG ("Returning closest prefix " << *closest->GetAddressPtr () << "");
	    }
	  return closest;
	}
      else
	{
	  Ptr<nnst::Entry> tmp = item->payload ();
//	  NS_LOG_DEBUG ("Found closest prefix. Returning the longest prefix " << *tmp->GetAddressPtr () << "");
	  return tmp;
	}
    }

    Ptr<nnst::Entry>
    NNST::ClosestSector (Ptr<const NNNAddress> prefix)
    {
      return ClosestSector (*prefix);
    }

    Ptr<const NNNAddress>
    NNST::ClosestSectorNameInfo (const NNNAddress &prefix)
    {
//      NS_LOG_FUNCTION (this << prefix);
      Ptr<nnst::Entry> tmp = ClosestSector (prefix);
      if (tmp != 0)
	return tmp->GetAddressPtr();
      else
	return Create<const NNNAddress> ();
    }

    Ptr<const NNNAddress>
    NNST::ClosestSectorNameInfo (Ptr<const NNNAddress> prefix)
    {
//      NS_LOG_FUNCTION (this << *prefix);
      return ClosestSectorNameInfo(*prefix);
    }

    std::pair<Ptr<Face>, Address>
    NNST::ClosestSectorFaceInfo (const NNNAddress &prefix, uint32_t skip)
    {
//        NS_LOG_FUNCTION (this << prefix);
        // Find the closest entry to prefix.
        Ptr<nnst::Entry> tmp = ClosestSector(prefix);

        if (tmp == 0)
          {
            Ptr<Face> ret = 0;
            return std::make_pair (ret, Address ());
          }
        else
          return tmp->FindBestCandidateFaceInfo(skip);
    }

    std::pair<Ptr<Face>, Address>
    NNST::ClosestSectorFaceInfo (Ptr<const NNNAddress> prefix, uint32_t skip)
    {
//        NS_LOG_FUNCTION (this << *prefix);
        return ClosestSectorFaceInfo(*prefix, skip);
    }

    std::pair<Ptr<Face>, Address>
    NNST::ClosestSectorFaceInfo (const NNNAddress &prefix, Ptr<Face> exclude, uint32_t skip)
    {
//      NS_LOG_FUNCTION (this << prefix);
      // Find the closest entry to prefix.
      Ptr<nnst::Entry> tmp = ClosestSector(prefix);

      NS_LOG_DEBUG ("Closest address was " << tmp->GetAddress());

      if (tmp == 0)
	{
	  Ptr<Face> ret = 0;
	  return std::make_pair (ret, Address ());
	}
      else
	return tmp->FindBestCandidateFaceInfo(exclude, skip);
    }

    std::pair<Ptr<Face>, Address>
    NNST::ClosestSectorFaceInfo (Ptr<const NNNAddress> prefix, Ptr<Face> exclude, uint32_t skip)
    {
//      NS_LOG_FUNCTION (this << *prefix);
      return ClosestSectorFaceInfo(*prefix, exclude, skip);
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopNameInfo (const NNNAddress &prefix)
    {
//      NS_LOG_FUNCTION (this << prefix);
      Ptr<nnst::Entry> curr;
      std::vector<Ptr<const NNNAddress> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1)
	    {
	      ret.push_back(curr->GetAddressPtr());
	    }
	}

      return ret;
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopNameInfo (Ptr<const NNNAddress> prefix, Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<Ptr<const NNNAddress> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(*prefix) == 1)
	    {
	      if (curr->ContainsFace (face))
		{
		  ret.push_back(curr->GetAddressPtr ());
		}
	    }
	}

      return ret;
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopNameInfo (Ptr<const NNNAddress> prefix)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopNameInfo (*prefix);
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopFaceInfo (const NNNAddress &prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<std::pair<Ptr<Face>, Address> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1)
	    {
	      ret.push_back(curr->FindBestCandidateFaceInfo(skip));
	    }
	}

      return ret;
    }

    std::set<Address>
    NNST::OneHopPoAInfo (Ptr<const NNNAddress> prefix, Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::set<Address> ret;
      std::vector<Address> tmp;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(*prefix) == 1)
	    {
	      if (curr->ContainsFace (face))
		{
		  tmp = curr->GetPoAs(face);
		  ret.insert (tmp.begin (), tmp.end ());
		}
	    }
	}

      return ret;
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopFaceInfo (Ptr<const NNNAddress> prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopFaceInfo(*prefix, skip);
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopSubSectorNameInfo (const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<Ptr<const NNNAddress> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1 && prefix.isParentSector(curr->GetAddress()))
	    {
	      ret.push_back(curr->GetAddressPtr());
	    }
	}

      return ret;
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopSubSectorNameInfo (Ptr<const NNNAddress> prefix)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopSubSectorNameInfo (*prefix);
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopSubSectorFaceInfo (const NNNAddress &prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<std::pair<Ptr<Face>, Address> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1 && prefix.isParentSector(curr->GetAddress()))
	    {
	      ret.push_back(curr->FindBestCandidateFaceInfo(skip));
	    }
	}

      return ret;
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopSubSectorFaceInfo (Ptr<const NNNAddress> prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopSubSectorFaceInfo(*prefix, skip);
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopParentSectorNameInfo (const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<Ptr<const NNNAddress> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1 && curr->GetAddress().isParentSector(prefix))
	    {
	      ret.push_back(curr->GetAddressPtr ());
	    }
	}

      return ret;
    }

    std::vector<Ptr<const NNNAddress> >
    NNST::OneHopParentSectorNameInfo (Ptr<const NNNAddress> prefix)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopParentSectorNameInfo(*prefix);
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopParentSectorFaceInfo (const NNNAddress &prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> curr;
      std::vector<std::pair<Ptr<Face>, Address> > ret;

      for (curr = Begin(); curr != End (); curr = Next(curr))
	{
	  if (curr->GetAddress().distance(prefix) == 1 && curr->GetAddress().isParentSector(prefix))
	    {
	      ret.push_back(curr->FindBestCandidateFaceInfo(skip));
	    }
	}

      return ret;
    }

    std::vector<std::pair<Ptr<Face>, Address> >
    NNST::OneHopParentSectorFaceInfo (Ptr<const NNNAddress> prefix, uint32_t skip)
    {
      NS_LOG_FUNCTION (this << *prefix);
      return OneHopParentSectorFaceInfo(*prefix, skip);
    }

    Ptr<nnst::Entry>
    NNST::Find (const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);
      super::iterator item = super::find_exact (prefix);

      if (item == super::end ())
	return 0;
      else
	return item->payload ();
    }

    Ptr<nnst::Entry>
    NNST::Find (Ptr<const NNNAddress> prefix)
    {
      return Find (*prefix);
    }

    void
    NNST::Add (const NNNAddress &name, Ptr<Face> face, Address poa, Time lease_expire, int32_t metric, bool fixed)
    {
      NS_LOG_FUNCTION ("const NNNAddress Add" << name << lease_expire);
      // We assume that the lease time gives us the absolute expiry time
      // We need to calculate the relative time for the Schedule function
      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for " << name << " at " << now);

      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  char c = 'a';
	  bool tmp = fullAdd (Create<NNNAddress> (name), face, poa, lease_expire, metric, c, fixed);

	  if (tmp)
	    {
	      if (!fixed)
		{
		  NS_LOG_DEBUG ("Scheduling expiration check for " << name << " Face " << face->GetId () << " PoA: " << poa  << " at " << lease_expire);
		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpiredName, this, Create<NNNAddress> (name));
		}
	      else
		{
		  NS_LOG_DEBUG (name << " entry is fixed, no need for expiration check");
		}
	    }
	  else
	    NS_LOG_DEBUG ("No changes to expiration check");
	}
      else
	NS_LOG_DEBUG ("Remaining lease time is negative, returning");
    }

    void
    NNST::Add (const Ptr<const NNNAddress> &prefix, std::vector<Ptr<Face> > faces, Address poa, Time lease_expire, int32_t metric, bool fixed)
    {
      NS_LOG_FUNCTION ("Face vector Add" << boost::cref(*prefix) << lease_expire);

      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for " << boost::cref(*prefix) << " at " << now);
      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  bool tmp = false;
	  char c = 'a';
	  for (std::vector<Ptr<Face> >::iterator i = faces.begin(); i != faces.end (); ++i)
	    {
	      tmp |= fullAdd(prefix, *i, poa, lease_expire, metric, c, fixed);
	    }

	  if (tmp)
	    {
	      if (!fixed)
		{
		  NS_LOG_DEBUG ("Scheduling expiration check for " << boost::cref(*prefix) << " with list of faces, PoA: " << poa  << " at " << lease_expire);
		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpiredName, this, prefix);
		}
	      else
		{
		  NS_LOG_DEBUG (*prefix << " entry is fixed, no need for expiration check");
		}
	    }
	  else
	    NS_LOG_DEBUG ("No changes to expiration check");
	}
      else
	NS_LOG_DEBUG ("Remaining lease time is negative, returning");
    }

    void
    NNST::Add (const Ptr<const NNNAddress> &prefix, Ptr<Face> face, std::vector<Address> poas, Time lease_expire, int32_t metric, bool fixed)
    {
      NS_LOG_FUNCTION ("Address vector Add" << boost::cref(*prefix) << lease_expire);

      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for " << boost::cref(*prefix) << " at " << now);
      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  bool tmp = false;
	  char c = 'a';
	  for (std::vector<Address>::iterator i = poas.begin(); i != poas.end (); ++i)
	    {
	      tmp |= fullAdd(prefix, face, *i, lease_expire, metric, c, fixed);
	    }

	  if (tmp)
	    {
	      if (!fixed)
		{
		  NS_LOG_DEBUG ("Scheduling expiration check for " << boost::cref(*prefix) << " Face " << face->GetId () << " with list of PoAs at " << lease_expire);
		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpiredName, this, prefix);
		}
	      else
		{
		  NS_LOG_DEBUG (*prefix << " entry is fixed, no need for expiration check");
		}
	    }
	  else
	    NS_LOG_DEBUG ("No changes to expiration check");
	}
      else
	NS_LOG_DEBUG ("Remaining lease time is negative, returning");
    }

    void
    NNST::Add (const Ptr<const NNNAddress> &name, Ptr<Face> face, Address poa, Time lease_expire, int32_t metric, bool fixed)
    {
      NS_LOG_FUNCTION ("Unitary Add" << boost::cref(*name) << lease_expire);

      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for " << boost::cref(*name) << " at " << now);
      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  char c = 'a';
	  bool tmp = fullAdd(name, face, poa, lease_expire, metric, c, fixed);

	  if (tmp)
	    {
	      if (!fixed)
		{
		  NS_LOG_DEBUG ("Scheduling expiration check for " << boost::cref(*name) << " Face " << face->GetId () << " PoA: " << poa << " at " << lease_expire);
		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpiredName, this, name);
		}
	      else
		{
		  NS_LOG_DEBUG (*name << " entry is fixed, no need for expiration check");
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("No changes to expiration check");
	    }
	}
      else
	NS_LOG_DEBUG ("Remaining lease time is negative, returning");
    }

    void
    NNST::UpdateStatus(const NNNAddress &prefix, Ptr<Face> face, nnst::FaceMetric::Status status)
    {
      NS_LOG_FUNCTION (this << prefix << boost::cref(*face) << status);

      super::iterator item = super::find_exact (prefix);

      if (item != super::end ())
	super::modify (&(*item), ll::bind (&nnst::Entry::UpdateStatus, ll::_1, face, status));
    }

    void
    NNST::UpdateLeaseTime(const NNNAddress &prefix, Time n_lease)
    {
      NS_LOG_FUNCTION (this << prefix << n_lease);

      Time now = Simulator::Now ();
      Time relativeExpireTime = n_lease - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for " << prefix << " at " << now);
      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  super::iterator item = super::find_exact (prefix);

	  if (item != super::end ())
	    {
	      bool ok = super::modify (&(*item), ll::bind (&nnst::Entry::UpdateLeaseTime, ll::_1, n_lease));

	      if (ok)
		{
		  Ptr<nnst::Entry> tmp = item->payload ();
//		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpired, this, tmp);
		  NS_LOG_DEBUG ("Scheduling expiration check for " << prefix << " after lease update to " << n_lease);
		  Simulator::Schedule(relativeExpireTime, &NNST::cleanExpiredName, this, Create<NNNAddress> (prefix));
		}
	    }
	}
    }

    Time
    NNST::GetLeaseTime(const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);

      Ptr<nnst::Entry> found = Find (prefix);

      if (found != 0)
	{
	  return found->GetLeaseTime ();
	}
      else
	{
	  return Seconds (0);
	}
    }

    void
    NNST::AddOrUpdateRoutingMetric(const NNNAddress &prefix, Ptr<Face> face, int32_t metric)
    {
      NS_LOG_FUNCTION (this << prefix << boost::cref(*face) << metric);
      super::iterator item = super::find_exact (prefix);

      if (item != super::end ())
	  super::modify (&(*item), ll::bind (&nnst::Entry::AddOrUpdateRoutingMetric, ll::_1, face, metric));
    }

    void
    NNST::UpdateFaceRtt(const NNNAddress &prefix, Ptr<Face> face, const Time &sample)
    {
      NS_LOG_FUNCTION (this << prefix << boost::cref(*face) << sample);
      super::iterator item = super::find_exact (prefix);

      if (item != super::end ())
	super::modify (&(*item), ll::bind (&nnst::Entry::UpdateFaceRtt, ll::_1, face, sample));
    }


    void
    NNST::InvalidateAll ()
    {
      NS_LOG_FUNCTION (this);

      super::parent_trie::recursive_iterator item (super::getTrie ());
      super::parent_trie::recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  super::modify (&(*item),
	                 ll::bind (&nnst::Entry::Invalidate, ll::_1));
	}
    }

    void
    NNST::Remove (const Ptr<const NNNAddress> &name)
    {
      NS_LOG_FUNCTION (boost::cref(*name));

      super::iterator nnstEntry = super::find_exact (*name);
      if (nnstEntry != super::end ())
	{
	  // notify forwarding strategy about soon be removed entry
	  //NS_ASSERT (this->GetObject<ForwardingStrategy> () != 0);
	  //this->GetObject<ForwardingStrategy> ()->WillRemoveNNSTEntry (nnstEntry->payload ());

	  super::erase (nnstEntry);
	}
    }

    void
    NNST::RemoveFromAll (Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this << boost::cref(*face));

      Ptr<nnst::Entry> entry = Begin ();
      while (entry != End ())
	{
	  entry->RemoveFace (face);
	  if (entry->m_faces.size () == 0)
	    {
	      Ptr<nnst::Entry> nextEntry = Next (entry);

	      // notify forwarding strategy about soon be removed FIB entry
	      //NS_ASSERT (this->GetObject<ForwardingStrategy> () != 0);
	      //this->GetObject<ForwardingStrategy> ()->WillRemoveNNSTEntry (entry);

	      super::erase (StaticCast<nnst::Entry> (entry)->to_iterator ());
	      entry = nextEntry;
	    }
	  else
	    {
	      entry = Next (entry);
	    }
	}
    }

    void
    NNST::RemoveFromAll (Address poa)
    {
      NS_LOG_FUNCTION (this << poa);

      Ptr<nnst::Entry> entry = Begin ();
      while (entry != End ())
	{
	  entry->RemovePoA (poa);
	  if (entry->m_faces.size () == 0)
	    {
	      Ptr<nnst::Entry> nextEntry = Next (entry);

	      // notify forwarding strategy about soon be removed NNST entry
	      //NS_ASSERT (this->GetObject<ForwardingStrategy> () != 0);
	      //this->GetObject<ForwardingStrategy> ()->WillRemoveNNSTEntry (entry);

	      super::erase (entry->to_iterator ());
	      entry = nextEntry;
	    }
	  else
	    {
	      entry = Next (entry);
	    }
	}
    }

    void
    NNST::Print (std::ostream &os) const
    {
      // !!! unordered_set imposes "random" order of item in the same level !!!
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  os << *(item->payload ());
	}
    }

    void
    NNST::PrintByMetric () const
    {
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  item->payload ()->printByMetric ();
	}
    }

    void
    NNST::PrintByAddress () const
    {
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  item->payload ()->printByAddress ();
	}
    }

    void
    NNST::PrintByLease () const
    {
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  item->payload ()->printByLease();
	}
    }

    void
    NNST::PrintByFace () const
    {
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;

	  item->payload ()->printByFace();
	}
    }

    uint32_t
    NNST::GetSize ()
    {
      return super::getPolicy ().size ();
    }

    bool
    NNST::isEmpty ()
    {
      return (super::getPolicy ().size () == 0);
    }

    Ptr<const nnst::Entry>
    NNST::Begin () const
    {
      super::parent_trie::const_recursive_iterator item (super::getTrie ());
      super::parent_trie::const_recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;
	  break;
	}

      if (item == end)
	return End ();
      else
	return item->payload ();
    }

    Ptr<nnst::Entry>
    NNST::Begin ()
    {
      super::parent_trie::recursive_iterator item (super::getTrie ());
      super::parent_trie::recursive_iterator end (0);
      for (; item != end; item++)
	{
	  if (item->payload () == 0) continue;
	  break;
	}

      if (item == end)
	return End ();
      else
	return item->payload ();
    }

    Ptr<const nnst::Entry>
    NNST::End () const
    {
      return 0;
    }

    Ptr<nnst::Entry>
    NNST::End ()
    {
      return 0;
    }

    Ptr<const nnst::Entry>
    NNST::Next (Ptr<const nnst::Entry> from) const
    {
      if (from == 0) return 0;

      super::parent_trie::const_recursive_iterator item (*StaticCast<const nnst::Entry> (from)->to_iterator ());
      super::parent_trie::const_recursive_iterator end (0);
      for (item++; item != end; item++)
	{
	  if (item->payload () == 0) continue;
	  break;
	}

      if (item == end)
	return End ();
      else
	return item->payload ();
    }

    Ptr<nnst::Entry>
    NNST::Next (Ptr<nnst::Entry> from)
    {
      if (from == 0) return 0;

      super::parent_trie::recursive_iterator item (*StaticCast<nnst::Entry> (from)->to_iterator ());
      super::parent_trie::recursive_iterator end (0);
      for (item++; item != end; item++)
	{
	  if (item->payload () == 0) continue;
	  break;
	}

      if (item == end)
	return End ();
      else
	return item->payload ();
    }

    Ptr<NNST>
    NNST::GetNNST (Ptr<Object> node)
    {
      return node->GetObject<NNST> ();
    }

    bool
    NNST::FoundName (const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);
      super::iterator item = super::find_exact (prefix);

      if (item == super::end ())
	return false;
      else
	return true;
    }

    bool
    NNST::FoundName (Ptr<const NNNAddress> prefix)
    {
      return FoundName (*prefix);
    }

    bool
    NNST::FoundPoAs (std::vector<Address> check)
    {
      NS_LOG_FUNCTION (this);

      Ptr<nnst::Entry> entry = Begin ();
      while (entry != End ())
	{
	  if (entry->EqualPoAs (check))
	    return true;

	  entry = Next (entry);
	}

      return false;
    }

    bool
    NNST::EqualPoAs(const NNNAddress &prefix, std::vector<Address> check)
    {
      NS_LOG_FUNCTION (this << prefix);

      super::iterator item = super::find_exact (prefix);

      if (item == super::end ())
	return false;
      else
	return item->payload ()->EqualPoAs (check);
    }

    std::vector<Address>
    NNST::GetAllPoas (const NNNAddress &prefix)
    {
      NS_LOG_FUNCTION (this << prefix);
      super::iterator item = super::find_exact (prefix);

      if (item == super::end ())
	return std::vector<Address> ();
      else
	return item->payload ()->GetPoAs();
    }

    void
    NNST::NotifyNewAggregate ()
    {
      Object::NotifyNewAggregate ();
    }

    void
    NNST::DoDispose (void)
    {
      clear ();
      Object::DoDispose ();
    }

    bool
    NNST::fullAdd (const Ptr<const NNNAddress> &name, Ptr<Face> face, Address poa, Time lease_expire, int32_t metric, char c, bool fixed)
    {
      NS_LOG_FUNCTION ("Full Add" << boost::cref(*name) << boost::cref(*face) << poa << lease_expire << metric);

      // ret only returns true for new entries, or new lease expiry times
      bool ret = false;
      // will add entry if doesn't exists, or just return an iterator to the existing entry
      std::pair< super::iterator, bool > result = super::insert (*name, 0);
      if (result.first != super::end ())
	{
	  if (result.second)
	    {
	      NS_LOG_DEBUG ("Completely new entry addition");
	      // The entry for this 3N name was newly created. Create a nnst::Entry
	      Ptr<nnst::Entry> newEntry = Create<nnst::Entry> (this, name);

	      newEntry->AddPoA(face, poa, lease_expire, metric);
	      newEntry->SetTrie (result.first);
	      newEntry->SetFixed (fixed);
	      result.first->set_payload (newEntry);

	      ret = true;
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Updating entry");
	      // The entry for this 3N name already exists. Need to update information
	      Time old_time = result.first->payload ()->GetLeaseTime ();

	      result.first->payload ()->UpdatePoA (face, poa, lease_expire, metric);

	      Time new_time = result.first->payload ()->GetLeaseTime ();

	      if (old_time != new_time)
		{
		  NS_LOG_DEBUG ("There was a modification on the prior check of lease time: " << old_time << " new time check " << new_time);
		  ret = true;
		}
	    }

	  // Update the NNST structure itself
	  super::modify (result.first,
	                 ll::bind (&nnst::Entry::AddOrUpdateRoutingMetric, ll::_1, face, metric));

	  return ret;
	}
      else
	{
	  NS_LOG_WARN ("Insertion failed. Check code.");
	  return ret;
	}
    }

    void
    NNST::RemoveFace (super::parent_trie &item, Ptr<Face> face)
    {
      if (item.payload () == 0) return;
      NS_LOG_FUNCTION (this);

      super::modify (&item,
                     ll::bind (&nnst::Entry::RemoveFace, ll::_1, face));
    }

    void
    NNST::RemovePoA (super::parent_trie &item, Address poa)
    {
      if (item.payload () == 0) return;
      NS_LOG_FUNCTION (this);

      super::modify (&item,
                     ll::bind (&nnst::Entry::RemovePoA, ll::_1, poa));
    }

    void
    NNST::cleanExpired(Ptr<nnst::Entry> item)
    {
      NS_LOG_FUNCTION (this << item);

      // Check if the item we have is actually valid (schedule issues)
      if (item)
	{
	  Ptr<const NNNAddress> name = item->GetAddressPtr();

	  if (!item->IsFixed ())
	    {
	      item->cleanExpired ();

	      if (item->isEmpty ())
		{
		  NS_LOG_INFO ("Removing NNST entry for " << *name << ", executing traced callback");
		  m_name_expired (name);
		  item->GetNNST () ->Remove (name);
		}
	    }
	}
    }

    void
    NNST::cleanExpiredName (Ptr<const NNNAddress> name)
    {
      NS_LOG_FUNCTION ("Checking expiration of " << boost::cref(*name));

      NS_LOG_DEBUG ("Before " << *this);
      super::iterator item = super::find_exact (*name);

      if (item != super::end ())
	{
	  Ptr<nnst::Entry> tmp = item->payload();
	  if (!tmp->IsFixed ())
	    {
	      tmp->cleanExpired();

	      if (tmp->isEmpty())
		{
		  NS_LOG_INFO ("Removing NNST entry for " << *name);
		  m_name_expired (name);
		  Remove (name);
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Entry for " << *name << " is fixed, returning");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Found no entry for " << boost::cref(*name));
	}

      NS_LOG_DEBUG ("After " << *this);
    }

    std::ostream&
    operator<< (std::ostream& os, const NNST &nnst)
    {
      os << "NNST:" << std::endl;
      os << "-------------------------------------------------------------------------------" << std::endl;
      os << boost::format(nnst_formatter) % "3N" % "PoA" % "LS" % "Face" % "S" % "C" % "M";
      os << "-------------------------------------------------------------------------------" << std::endl;
      nnst.Print (os);
      os << "-------------------------------------------------------------------------------" << std::endl;
      return os;
    }

  } /* namespace nnn */
} /* namespace ns3 */
