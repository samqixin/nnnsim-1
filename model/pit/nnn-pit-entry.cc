/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pit-entry.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pit-entry.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pit-entry.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pit-entry.h"

#include "ns3/nnn-pit.h"
#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"

#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-icn-pdus.h"

#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/foreach.hpp>
namespace ll = boost::lambda;

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.pit.Entry");

  namespace nnn
  {
    namespace pit
    {
      Entry::Entry (Pit &container,
                    Ptr<const Interest> header,
                    Ptr<fib::Entry> fibEntry)
      : m_container (container)
      , m_interest (header)
      , m_fibEntry (fibEntry)
      , m_maxRetxCount (0)
      , m_currRetries (0)
      {
	NS_LOG_FUNCTION (this);

	// UpdateLifetime is (and should) be called from the forwarding strategy

	UpdateLifetime ((!header->GetInterestLifetime ().IsZero ()?
	    header->GetInterestLifetime ():
	    Seconds (1.0)));
      }

      Entry::~Entry ()
      {
	NS_LOG_FUNCTION (GetPrefix ());
      }

      void
      Entry::UpdateLifetime (const Time &offsetTime)
      {
	NS_LOG_FUNCTION (this);

	Time newExpireTime = Simulator::Now () + (m_container.GetMaxPitEntryLifetime ().IsZero () ?
	    offsetTime :
	    std::min (m_container.GetMaxPitEntryLifetime (), offsetTime));
	if (newExpireTime > m_expireTime)
	  m_expireTime = newExpireTime;

	NS_LOG_INFO (this->GetPrefix () << ", Updated lifetime to " << m_expireTime.ToDouble (Time::S) << "s, " << (m_expireTime-Simulator::Now ()).ToDouble (Time::S) << "s left");
      }

      void
      Entry::OffsetLifetime (const Time &offsetTime)
      {
	m_expireTime += offsetTime;
	if (m_expireTime < Simulator::Now ())
	  {
	    m_expireTime = Simulator::Now ();
	  }
	NS_LOG_INFO (this->GetPrefix () << ", Offsetting lifetime to " << m_expireTime.ToDouble (Time::S) << "s, " << (m_expireTime-Simulator::Now ()).ToDouble (Time::S) << "s left");
      }

      const icn::Name &
      Entry::GetPrefix () const
      {
	return m_interest->GetName ();
      }

      const Time &
      Entry::GetExpireTime () const
      {
	return m_expireTime;
      }

      bool
      Entry::IsNonceSeen (uint32_t nonce) const
      {
	return m_seenNonces.find (nonce) != m_seenNonces.end ();
      }

      void
      Entry::AddSeenNonce (uint32_t nonce)
      {
	m_seenNonces.insert (nonce);
      }

      Entry::in_iterator
      Entry::AddIncoming (Ptr<Face> face)
      {
	if (!face)
	  return m_incoming.end ();

	NS_LOG_DEBUG ("Introducing Face " << face->GetId ());

	std::pair<in_iterator,bool> ret =
	    m_incoming.insert (IncomingFace (face));

	// NS_ASSERT_MSG (ret.second, "Something is wrong");

	return ret.first;
      }

      Entry::in_iterator
      Entry::AddIncoming(Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	if (!face)
	  return m_incoming.end ();

	NS_LOG_DEBUG ("Introducing Face " << face->GetId ());

	std::pair<in_iterator, bool> ret = m_incoming.insert (IncomingFace(face, addr));

	if (!ret.second)
	  { // Incoming face already exists
	    const_cast<IncomingFace&>(*ret.first).AddDestination(addr);
	  }

	return ret.first;
      }

      Entry::in_iterator
      Entry::AddIncoming(Ptr<Face> face, bool raw_icn)
      {
	if (!face)
	  return m_incoming.end ();

	NS_LOG_DEBUG ("Introducing Face " << face->GetId ());

	std::pair<in_iterator, bool> ret = m_incoming.insert (IncomingFace(face, raw_icn));

	if (!ret.second)
	  { // Incoming face already exists
	    const_cast<IncomingFace&>(*ret.first).EnableRawICN(raw_icn);
	  }

	return ret.first;
      }

      Entry::in_iterator
      Entry::AddIncoming (Ptr<Face> face, Address addr, bool raw_icn, bool saw_null)
      {
	if (!face)
	  return m_incoming.end ();

	NS_LOG_DEBUG ("Introducing Face " << face->GetId () << " using PoA: " << addr);

	std::pair<in_iterator, bool> ret = m_incoming.insert (IncomingFace(face, addr, raw_icn, saw_null));

	if (!ret.second)
	  { // Incoming face already exists
	    const_cast<IncomingFace&>(*ret.first).EnableRawICNonPoA(addr, raw_icn);
	    const_cast<IncomingFace&>(*ret.first).EnableSawNULLonPoA(addr, saw_null);
	  }

	return ret.first;
      }

      Entry::out_iterator
      Entry::AddOutgoingDestination (Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	if (!face)
	  return m_outgoing.end ();

	NS_LOG_DEBUG ("Introducing Face " << face->GetId () << " with 3N Dst: " << *addr);

	std::pair<out_iterator, bool> ret = m_outgoing.insert (OutgoingFace (face));

	if (!ret.second)
	  { // Outgoing face already exists
	    const_cast<OutgoingFace&>(*ret.first).Add3NDestination(addr);
	  }

	return ret.first;
      }

      void
      Entry::EnableRawICN (Ptr<Face> face, bool enable)
      {
	if (!face)
	  return;

	std::pair<in_iterator, bool> ret = m_incoming.insert (IncomingFace(face, enable));

	if (!ret.second)
	  { // Incoming face already exists
	    const_cast<IncomingFace&>(*ret.first).EnableRawICN(enable);
	  }
      }

      bool
      Entry::SawRawICN (Ptr<Face> face) const
      {
	if (!face)
	  return false;

	in_iterator it = m_incoming.find(face);

	if (it != m_incoming.end())
	  {
	    return const_cast<IncomingFace&>(*it).SawRawICN();
	  }
	else
	  {
	    return false;
	  }
      }

      void
      Entry::EnableSawNULL (Ptr<Face> face, bool enable)
      {
	if (!face)
	  return;

	in_iterator it = m_incoming.find (face);

	if (it != m_incoming.end ())
	  { // Incoming face already exists
	    const_cast<IncomingFace&>(*it).EnableSawNULL(enable);
	  }
      }

      bool
      Entry::SawNULL (Ptr<Face> face) const
      {
	if (!face)
	  return false;

	in_iterator it = m_incoming.find(face);

	if (it != m_incoming.end())
	  {
	    return const_cast<IncomingFace&>(*it).SawNULL();
	  }
	else
	  {
	    return false;
	  }
      }

      bool
      Entry::IncomingDestinationExists (Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	in_iterator it = m_incoming.find(face);

	if (it != m_incoming.end())
	  {
	    IncomingFace inface = const_cast<IncomingFace&>(*it);

	    return inface.DestinationExists(addr);
	  }
	else
	  {
	    NS_LOG_WARN ("Did not find Face " << face->GetId ());
	    return false;
	  }
      }

      bool
      Entry::OutgoingDestinationExists (Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	out_iterator it = m_outgoing.find(face);

	if (it != m_outgoing.end())
	  {
	    OutgoingFace outface = const_cast<OutgoingFace&>(*it);

	    return outface.Has3NDestination(addr);
	  }
	else
	  {
	    NS_LOG_WARN ("Did not find Face " << face->GetId ());
	    return false;
	  }
      }

      void
      Entry::RemoveIncoming (Ptr<Face> face)
      {
	in_iterator it = m_incoming.find(face);

	if (it != m_incoming.end())
	  {
	    if (const_cast<IncomingFace&>(*it).NoAddresses())
	      m_incoming.erase(face);
	  }
      }

      void
      Entry::RemoveIncoming (Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	in_iterator it = m_incoming.find(face);

	if (it != m_incoming.end())
	  {
	    IncomingFace inface = const_cast<IncomingFace&>(*it);

	    inface.RemoveDestination(addr);

	    if (inface.NoAddresses())
	      m_incoming.erase(face);
	  }
      }

      void
      Entry::RemoveOutgoing (Ptr<Face> face, Ptr<const NNNAddress> addr)
      {
	out_iterator it = m_outgoing.find(face);

	if (it != m_outgoing.end())
	  {
	    OutgoingFace outface = const_cast<OutgoingFace&>(*it);

	    outface.Remove3NDestination(addr);

	    if (outface.NoAddresses())
	      m_outgoing.erase(face);
	  }
      }

      void
      Entry::ClearIncoming ()
      {
	m_incoming.clear ();
      }

      Entry::out_iterator
      Entry::AddOutgoing (Ptr<Face> face)
      {
	std::pair<out_iterator,bool> ret =
	    m_outgoing.insert (OutgoingFace (face));

	if (!ret.second)
	  { // outgoing face already exists
	    const_cast<OutgoingFace&>(*ret.first).UpdateOnRetransmit ();
	    // m_outgoing.modify (ret.first,
	    //                    ll::bind (&OutgoingFace::UpdateOnRetransmit, ll::_1));
	  }

	return ret.first;
      }

      Entry::out_iterator
      Entry::AddOutgoing (Ptr<Face> face, Address addr, bool raw_icn)
      {
	std::pair<out_iterator,bool> ret =
	    m_outgoing.insert (OutgoingFace (face, addr, raw_icn));

	if (!ret.second)
	  { // outgoing face already exists
	    const_cast<OutgoingFace&>(*ret.first).UpdateOnRetransmit (addr, raw_icn);
	  }

	return ret.first;
      }

      void
      Entry::RemoveOutgoing (Ptr<Face> face)
      {
	out_iterator it = m_outgoing.find(face);

	if (it != m_outgoing.end())
	  {
	    m_outgoing.erase(face);
	  }
      }

      void
      Entry::ClearOutgoing ()
      {
	m_outgoing.clear ();
      }

      void
      Entry::RemoveAllReferencesToFace (Ptr<Face> face)
      {
	in_iterator incoming = m_incoming.find (face);

	if (incoming != m_incoming.end ())
	  m_incoming.erase (incoming);

	out_iterator outgoing =
	    m_outgoing.find (face);

	if (outgoing != m_outgoing.end ())
	  m_outgoing.erase (outgoing);
      }

      void
      Entry::SetWaitingInVain (Ptr<Face> face)
      {
	out_iterator item = m_outgoing.find (face);
	if (item == m_outgoing.end ())
	  return;

	const_cast<OutgoingFace&>(*item).m_waitingInVain = true;
      }

      bool
      Entry::AreAllOutgoingInVain () const
      {
	NS_LOG_DEBUG (m_outgoing.size ());

	bool inVain = true;
	std::for_each (m_outgoing.begin (), m_outgoing.end (),
	               ll::var(inVain) &= (&ll::_1)->*&OutgoingFace::m_waitingInVain);

	NS_LOG_DEBUG ("inVain " << inVain);
	return inVain;
      }

      bool
      Entry::AreTherePromisingOutgoingFacesExcept (Ptr<Face> face) const
      {
	bool inVain = true;
	std::for_each (m_outgoing.begin (), m_outgoing.end (),
	               ll::var(inVain) &=
	        	   ((&ll::_1)->*&OutgoingFace::m_face == face ||
	        	       (&ll::_1)->*&OutgoingFace::m_waitingInVain));

	return !inVain;
      }

      void
      Entry::IncreaseAllowedRetxCount ()
      {
	if (Simulator::Now () - m_lastRetransmission >= MilliSeconds (100))
	  {
	    // cheat:
	    // don't allow retransmission faster than every 100ms
	    m_maxRetxCount++;
	    m_lastRetransmission = Simulator::Now ();
	  }
      }

      void
      Entry::IncreaseCurrentRetransmissions ()
      {
	m_currRetries++;
      }

      uint32_t
      Entry::GetCurrentRetransmissions ()
      {
	return m_currRetries;
      }

      Ptr<fib::Entry>
      Entry::GetFibEntry ()
      {
	return m_fibEntry;
      };

      const Entry::in_container &
      Entry::GetIncoming () const
      {
	return m_incoming;
      }

      uint32_t
      Entry::GetIncomingCount () const
      {
	return m_incoming.size ();
      }

      const Entry::out_container &
      Entry::GetOutgoing () const
      {
	return m_outgoing;
      }

      uint32_t
      Entry::GetOutgoingCount () const
      {
	return m_outgoing.size ();
      }

      uint32_t
      Entry::GetMaxRetxCount () const
      {
	return m_maxRetxCount;
      }

      Ptr<const Interest>
      Entry::GetInterest () const
      {
	return m_interest;
      }

      std::ostream& operator<< (std::ostream& os, const Entry &entry)
      {
	os << "Prefix: " << entry.GetPrefix () << "\n";
	os << "Incoming: " << entry.GetIncomingCount () << std::endl;
	os << "Outgoing: " << entry.GetOutgoingCount () << std::endl;

	os << "In: ";
	bool first = true;
	BOOST_FOREACH (const IncomingFace &face, entry.m_incoming)
	{
	  if (!first)
	    os << ",";
	  else
	    first = false;

	  os << *face.m_face;
	}

	os << "\nOut: ";
	first = true;
	BOOST_FOREACH (const OutgoingFace &face, entry.m_outgoing)
	{
	  if (!first)
	    os << ",";
	  else
	    first = false;

	  os << *face.m_face;
	}
	os << "\nNonces: ";
	first = true;
	BOOST_FOREACH (uint32_t nonce, entry.m_seenNonces)
	{
	  if (!first)
	    os << ",";
	  else
	    first = false;

	  os << nonce;
	}

	return os;
      }
    } // namespace pit
  } // namespace nnn
} // namespace ns3
