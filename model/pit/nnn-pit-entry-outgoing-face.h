/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pit-entry-outgoing-face.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pit-entry-outgoing-face.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pit-entry-outgoing-face.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_PIT_ENTRY_OUTGOING_FACE_H_
#define	_NNN_PIT_ENTRY_OUTGOING_FACE_H_

#include "ns3/nstime.h"
#include "ns3/ptr.h"
#include "ns3/simulator.h"

#include "ns3/nnn-face.h"
#include "ns3/nnn-naming.h"

#include <boost/format.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

namespace ns3
{
  namespace nnn
  {
    namespace pit
    {

      struct PoAOutInfo
      {
      public:
	PoAOutInfo () {
	  m_sendTime = Simulator::Now ();
	  m_raw_icn = false;
	  m_retxCount = 0;
	  m_waitingInVain = false;
	}

	Time m_sendTime;
	bool m_raw_icn;
	uint32_t m_retxCount;
	bool m_waitingInVain;
      };

      // Common way to order 3N names
      struct PtrNNNComp
      {
	bool operator () (const Ptr<const NNNAddress> &lhs , const Ptr<const NNNAddress>  &rhs) const  {
	  return *lhs < *rhs;
	}
      };

      struct NamesSeen
      {
	NamesSeen (Ptr<const NNNAddress> _name, Time _firstSeen, Time _lastSeen) : name (_name), firstSeen (_firstSeen), lastSeen (_lastSeen) { }

	bool operator< (const NamesSeen e) const
	{
	  if (lastSeen == e.lastSeen)
	    {
	      return firstSeen > e.firstSeen;
	    }
	  else
	    {
	      return (lastSeen > e.lastSeen);
	    }
	}

	Ptr<const NNNAddress> name;
	Time firstSeen;
	Time lastSeen;
      };

      class i_name { };
      class i_lastseen { };

      struct NamesSeenContainer :
	  public boost::multi_index::multi_index_container<
	      NamesSeen,
	      boost::multi_index::indexed_by<
	          // Sort by NameSeen structure
	          boost::multi_index::ordered_unique<
	          boost::multi_index::tag<i_lastseen>,
	          boost::multi_index::identity<NamesSeen>
              >,
              // Sort by 3N name
              boost::multi_index::ordered_unique<
                  boost::multi_index::tag<i_name>,
                  boost::multi_index::member<NamesSeen, Ptr<const NNNAddress>, &NamesSeen::name>,
                  PtrNNNComp
              >
          >
      > { };

      typedef NamesSeenContainer::index<i_name>::type names_seen_by_name;
      typedef NamesSeenContainer::index<i_lastseen>::type names_seen_by_time;

      /**
       * @ingroup ndn-pit
       * @brief PIT state component for each outgoing interest
       */
      struct OutgoingFace
      {
	Ptr<Face> m_face;     ///< \brief face of the outgoing Interest
	std::map<Address,PoAOutInfo> m_poa_addrs;
	Time m_sendTime;          ///< \brief time when the first outgoing interest is sent (for RTT measurements)
	///< \todo handle problem of retransmitted interests... Probably, we should include something similar
	///<       to TimeStamp TCP option for retransmitted (i.e., only lost interests will suffer)
	uint32_t m_retxCount;     ///< \brief number of retransmission
	bool m_waitingInVain;     ///< \brief when flag is set, we do not expect data for this interest, only a small hope that it will happen
	NamesSeenContainer m_dst_addrs;  ///< \brief Container for the 3N names used in the outgoing Interests

      public:
	/**
	 * @brief Constructor to create PitEntryOutgoingFace
	 * \param face face of the outgoing interest
	 */
	OutgoingFace (Ptr<Face> face);

	OutgoingFace (Ptr<Face> face, Address addr, bool raw_icn);

	/**
	 * @brief Default constructor, necessary for Python bindings, but should not be used anywhere else.
	 */
	OutgoingFace ();

	/**
	 * @brief Copy operator
	 */
	OutgoingFace &
	operator = (const OutgoingFace &other);

	/**
	 * @brief Update outgoing entry upon retransmission
	 */
	void
	UpdateOnRetransmit ();

	void
	UpdateOnRetransmit (Address addr, bool raw_icn);

	/**
	 * \brief Add the PoA name to the associated face
	 * \param addr Address we will aggregate
	 */
	void
	AddPoADestination (Address addr);

	std::map<Address,PoAOutInfo>
	GetPoADestinations () const;

	void
	Add3NDestination (Ptr<const NNNAddress> addr);

	bool
	Has3NDestination (Ptr<const NNNAddress> addr);

	void
	Remove3NDestination (Ptr<const NNNAddress> addr);

	bool
	NoAddresses ();

	/**
	 * \brief Remove the PoA name to the associated face
	 * \param addr Address we will remove
	 */
	void
	RemovePoADestination (Address addr);

	void
	EnableRawICNonPoA (Address addr);

	bool
	SawRawICNonPoA (Address addr);

	/**
	 * @brief Compare to PitEntryOutgoingFace
	 */
	bool operator== (const OutgoingFace &dst) { return *m_face==*dst.m_face; }

	/**
	 * @brief Compare PitEntryOutgoingFace with Face
	 */
	bool operator== (Ptr<Face> face) { return *m_face==*face; }

	/**
	 * \brief Comparison operator used by boost::multi_index::identity<>
	 */
	bool
	operator< (const OutgoingFace &m) const { return *m_face < *(m.m_face); } // return identity of the face
      };
    } // namespace pit
  } // namespace nnn
} // namespace ns3

#endif	/* NNN_PIT_ENTRY_OUTGOING_FACE_H */
