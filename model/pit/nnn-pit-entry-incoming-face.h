/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pit-entry-incoming-face.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pit-entry-incoming-face.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pit-entry-incoming-face.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_PIT_ENTRY_INCOMING_FACE_H_
#define	_NNN_PIT_ENTRY_INCOMING_FACE_H_

#include "ns3/nstime.h"
#include "ns3/ptr.h"
#include "ns3/simulator.h"

#include "ns3/nnn-face.h"
#include "ns3/nnn-addr-aggregator.h"

namespace ns3
{
  namespace nnn
  {
    class NNNAddress;

    namespace pit
    {
      struct PoAInInfo
      {

      public:
	PoAInInfo () {
	  m_arrivalTime = Simulator::Now ();
	  m_raw_icn = false;
	  m_saw_null = false;
	}

	Time m_arrivalTime;
	bool m_raw_icn;
	bool m_saw_null;
      };

      /**
       * @ingroup nnn-pit
       * @brief PIT state component for each incoming interest (not including duplicates)
       */
      struct IncomingFace
      {
	Ptr<Face> m_face; ///< \brief face of the incoming Interest
	Ptr<NNNAddrAggregator> m_addrs; ///< \brief 3N names seen in incoming Interests
	std::map<Address,PoAInInfo> m_poa_addrs; ///< \brief PoA names seen in incoming Interests
	Time m_arrivalTime;   ///< \brief arrival time of the incoming Interest
	bool m_raw_icn;       ///< \brief Flags if we have seen a raw ICN PDU
	bool m_saw_null;      ///< \bried Flags if we have seen a NULL 3N PDU

      public:
	/**
	 * \brief Constructor
	 * \param face face of the incoming interest
	 */
	IncomingFace (Ptr<Face> face);

	/**
	 * \brief Constructor
	 * \param face face of the incoming interest
	 * \param addr 3N name we will aggregate
	 */
	IncomingFace (Ptr<Face> face, Ptr<const NNNAddress> addr);

	IncomingFace (Ptr<Face> face, bool saw_icn);

	IncomingFace (Ptr<Face> face, Address addr);

	IncomingFace (Ptr<Face> face, Address addr, bool raw_icn, bool saw_null);

	/**
	 * @brief Default constructor, necessary for Python bindings, but should not be used anywhere else.
	 */
	IncomingFace ();

	/**
	 * \brief Add the 3N name to the associated face
	 * \param addr 3N name we will aggregate
	 */
	void
	AddDestination (Ptr<const NNNAddress> addr);

	/**
	 * \brief Add the 3N name to the associated face
	 * \param addr 3N name we will aggregate
	 */
	void
	RemoveDestination (Ptr<const NNNAddress> addr);

	/**
	 * \brief Add the PoA name to the associated face
	 * \param addr Address we will aggregate
	 */
	void
	AddPoADestination (Address addr);

	std::map<Address,PoAInInfo>
	GetPoADestinations () const;

	/**
	 * \brief Remove the PoA name to the associated face
	 * \param addr Address we will remove
	 */
	void
	RemovePoADestination (Address addr);

	bool
	NoAddresses ();

	bool
	DestinationExists (Ptr<const NNNAddress> addr);

	void
	EnableRawICN (bool enable = true);

	bool
	SawRawICN () const;

	void
	EnableSawNULL (bool enable = true);

	bool
	SawNULL () const;

	void
	EnableRawICNonPoA (Address poa, bool enable = true);

	bool
	SawRawICNonPoA (Address poa);

	void
	EnableSawNULLonPoA (Address poa, bool enable = true);

	bool
	SawNULLonPoA (Address poa);

	/**
	 * @brief Copy operator
	 */
	IncomingFace &
	operator = (const IncomingFace &other);

	/**
	 * @brief Compare two PitEntryIncomingFace
	 */
	bool operator== (const IncomingFace &dst) const { return *m_face==*(dst.m_face); }

	/**
	 * @brief Compare PitEntryIncomingFace with Face
	 */
	bool operator== (Ptr<Face> face) const { return *m_face==*face; }

	/**
	 * \brief Comparison operator used by boost::multi_index::identity<>
	 */
	bool
	operator< (const IncomingFace &m) const { return *m_face < *(m.m_face); } // return identity of the face
      };
    } // namespace pit
  } // namespace nnn
} // namespace ns3

#endif	/* NNN_PIT_ENTRY_INCOMING_FACE_H */
