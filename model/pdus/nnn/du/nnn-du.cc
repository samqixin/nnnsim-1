/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-du.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-du.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-du.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"

#include "nnn-du.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.DU");

  namespace nnn
  {
    DU::DU ()
    : NNNPDU (DU_NNN, Seconds (0))
    , DATAPDU ()
    , m_authoritative (true)
    {
    }

    DU::DU (Ptr<const NNNAddress> src, Ptr<const NNNAddress> dst, Ptr<Packet> payload)
    : NNNPDU (DU_NNN, Seconds (0))
    , DATAPDU ()
    , m_authoritative (true)
    {
      if (m_payload == 0)
	m_payload = Create<Packet> ();
      else
	m_payload = payload;

      SetSrcName (src);
      SetDstName (dst);
    }

    DU::DU (Ptr<const NNNAddress> src, Ptr<const NNNAddress> dst, Ptr<Packet> payload,
    	bool authoritative)
    : NNNPDU (DU_NNN, Seconds(0))
    , DATAPDU ()
    , m_authoritative (authoritative)
    {
      if (m_payload == 0)
     	m_payload = Create<Packet> ();
           else
     	m_payload = payload;

      SetSrcName (src);
      SetDstName (dst);
    }

    DU::~DU ()
    {
    }


    DU::DU (const DU &du_p)
    {
      NS_LOG_FUNCTION("DU correct copy constructor");
      m_packetid = du_p.GetPacketId ();
      m_version = du_p.GetVersion ();
      m_ttl = du_p.GetLifetime ();
      m_PDUdatatype = du_p.GetPDUPayloadType ();
      if (du_p.GetPayload ())
	m_payload = du_p.GetPayload ()->Copy ();
      m_src_name = du_p.GetSrcNamePtr ();
      m_dst_name = du_p.GetDstNamePtr ();
      m_pdurf = du_p.GetPDURF ();
      m_window = du_p.GetPDUWindow ();
      m_rate = du_p.GetRate ();
      m_ini_seq = du_p.GetInitialWindowSeq ();
      m_flowid = du_p.GetFlowid ();
      m_sequence = du_p.GetSequence ();
      m_authoritative = du_p.IsAuthoritative ();
      if (du_p.GetWire ())
	m_wire = du_p.GetWire()->Copy();
    }

    const NNNAddress&
    DU::GetSrcName () const
    {
      if (m_src_name == 0) throw DUException ();
      return *m_src_name;
    }

    const NNNAddress&
    DU::GetDstName () const
    {
      if (m_dst_name == 0) throw DUException ();
      return *m_dst_name;
    }

    Ptr<const NNNAddress>
    DU::GetSrcNamePtr () const
    {
      return m_src_name;
    }

    Ptr<const NNNAddress>
    DU::GetDstNamePtr () const
    {
      return m_dst_name;
    }

    bool
    DU::IsAuthoritative () const
    {
      return m_authoritative;
    }

    void
    DU::SetAuthoritative (bool value)
    {
      m_authoritative = value;
    }

    void
    DU::Print (std::ostream &os) const
    {
      os << "<DU>" << std::endl;
      NNNPDU::Print (os);
      os << "  <Name Src>" << GetSrcName () << "</Name Src>" << std::endl;
      os << "  <Name Dst>" << GetDstName () << "</Name Dst>" << std::endl;
      os << "  <Authoritative>" << std::boolalpha << IsAuthoritative() << "</Authoritative>" << std::endl;
      DATAPDU::Print (os);
      os << "</DU>" << std::endl;
    }

    bool
    DU::operator < (const Ptr<DU>& du_p) const
    {
      if (GetFlowid () == du_p->GetFlowid ())
	{
	  return false;
	}

      if (GetSequence () == du_p->GetSequence () )
	{
	  if (GetSrcName () == du_p->GetSrcName ())
	    {
	      return (GetDstName () < du_p->GetDstName ());
	    }
	  else
	    {
	      return (GetSrcName () < du_p->GetSrcName ());
	    }
	}
      else
	{
	  return (GetSequence () < du_p->GetSequence ());
	}
    }

    Ptr<DATAPDU>
    DU::Copy () const
    {
      return Ptr<DATAPDU> (new DU(*this), false);
    }

  } /* namespace nnn */
} /* namespace ns3 */


