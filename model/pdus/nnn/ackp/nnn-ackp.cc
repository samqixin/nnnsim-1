/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-ackp.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-ackp.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-ackp.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#include "ns3/log.h"

#include "nnn-ackp.h"

namespace ns3
{
  namespace nnn
  {
    NS_LOG_COMPONENT_DEFINE ("nnn.ACKP");

    ACKP::ACKP ()
    : NNNPDU (ACKP_NNN, Seconds (0))
    , m_type (0)
    {

    }

    ACKP::ACKP (
	uint32_t packet_id,
	uint32_t flowid,
	uint32_t sequence
    )
    : NNNPDU (ACKP_NNN, Seconds (0))
    {
      AddACKFlowidSeq(flowid, sequence);
    }

    ACKP::~ACKP ()
    {

    }

    ACKP::ACKP (
	const ACKP& ack_p
	)
    {
      ACKP ();
      SetVersion (ack_p.GetVersion ());
      SetLifetime (ack_p.GetLifetime ());
      SetACKFlowids (ack_p.GetACKFlowids ());
    }

    void
    ACKP::SetOutofOrder (bool value)
    {
      if (value)
	m_type |= 1;
      else
	m_type &= 0;
    }

    uint8_t
    ACKP::GetType () const
    {
      return m_type;
    }

    void
    ACKP::SetType (uint8_t type)
    {
      m_type = type;
    }

    bool
    ACKP::IsOutofOrder () const
    {
      return (m_type && 1);
    }

    const std::map<uint32_t, uint32_t>
    ACKP::GetACKFlowids () const
    {
      return m_flowid_seq;
    }

    void
    ACKP::SetACKFlowids (std::map<uint32_t, uint32_t> other)
    {
      m_flowid_seq = other;
    }

    void
    ACKP::AddACKFlowidSeq (uint32_t flowid, uint32_t sequence)
    {
      m_flowid_seq.insert (std::pair<uint32_t,uint32_t>(flowid,sequence));
    }

    size_t
    ACKP::GetACKNumFlowids () const
    {
      return m_flowid_seq.size ();
    }
  } /* namespace nnn */
} /* namespace ns3 */
