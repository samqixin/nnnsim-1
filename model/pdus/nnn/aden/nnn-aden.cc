/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-aden.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-aden.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-aden.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"

#include "nnn-aden.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ADEN");

  namespace nnn
  {
    ADEN::ADEN ()
    : NNNPDU (ADEN_NNN, Seconds (0))
    , ENPDU ()
    , m_lease_expire (Seconds (0))
    {
    }

    ADEN::ADEN (Ptr<NNNAddress> name, Time lease_expire)
    : NNNPDU (ADEN_NNN, Seconds(0))
    , ENPDU ()
    , m_lease_expire (lease_expire)
    {
      SetName (name);
    }

    ADEN::ADEN (const NNNAddress &name, Time lease_expire)
    : NNNPDU (ADEN_NNN, Seconds(0))
    , ENPDU ()
    , m_lease_expire (lease_expire)
    {
      SetName (name);
    }

    ADEN::ADEN (const ADEN &aden_p)
    {
      NS_LOG_FUNCTION("ADEN correct copy constructor");
      ADEN ();
      SetVersion (aden_p.GetVersion ());
      SetLifetime (aden_p.GetLifetime ());
      SetSequence (aden_p.GetSequence ());
      AddPoa (aden_p.GetPoas ());
      SetName (aden_p.GetName ());
      SetExpireTime (aden_p.GetExpireTime ());
      SetWire (aden_p.GetWire ());
    }

    ADEN::~ADEN()
    {
      m_name = 0;
    }

    const NNNAddress&
    ADEN::GetName () const
    {
      if (m_name == 0) throw ADENException ();
      return *m_name;
    }

    Ptr<const NNNAddress>
    ADEN::GetNamePtr () const
    {
      return m_name;
    }

    void
    ADEN::SetName(Ptr<NNNAddress> name)
    {
      m_name = name;
      SetWire (0);
    }

    void
    ADEN::SetName (const NNNAddress &name)
    {
      m_name = Create<NNNAddress> (name);
      SetWire (0);
    }

    Time
    ADEN::GetExpireTime () const
    {
      return m_lease_expire;
    }

    void
    ADEN::SetExpireTime (Time expiry)
    {
      m_lease_expire = expiry;
    }

    void
    ADEN::Print (std::ostream &os) const
    {
      os << "<ADEN>" << std::endl;
      NNNPDU::Print(os);
      os << "  <Name>" << GetName () << "</Name>" << std::endl;
      os << "  <Lease>" << GetExpireTime () << "</Lease>" << std::endl;
      ENPDU::Print(os);
      os << "</ADEN>" << std::endl;
    }
  } /* namespace nnn */
} /* namespace ns3 */
