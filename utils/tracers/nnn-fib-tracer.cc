/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2018 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-fib-tracer.h"

#include "ns3/node.h"
#include "ns3/packet.h"
#include "ns3/config.h"
#include "ns3/names.h"
#include "ns3/callback.h"
#include "ns3/simulator.h"
#include "ns3/node-list.h"
#include "ns3/log.h"

#include "ns3/nnn-fib.h"

#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/make_shared.hpp>

NS_LOG_COMPONENT_DEFINE ("nnn.FibTracer");

namespace ns3
{
  namespace nnn
  {
    template<class T>
    static inline void
    NullDeleter (T *ptr)
    {
    }

    static std::list< boost::tuple< boost::shared_ptr<std::ostream>, std::list<Ptr<FibTracer> > > > g_tracers;

    FibTracer::FibTracer (boost::shared_ptr<std::ostream> os,
			  Ptr<Node> node)
    : m_nodePtr (node)
    , m_os (os)
    {
      m_node = boost::lexical_cast<std::string> (m_nodePtr->GetId ());

      Connect ();

      std::string name = Names::FindName (node);
      if (!name.empty ())
	{
	  m_node = name;
	}
    }

    FibTracer::FibTracer (boost::shared_ptr<std::ostream> os,
			  const std::string& node)
    : m_node (node)
    , m_os (os)
    {
      Connect ();
    }

    FibTracer::~FibTracer ()
    {
    }

    void
    FibTracer::Install (Ptr<Node> node, const std::string& file)
    {
      std::list<Ptr<FibTracer> > tracers;
      boost::shared_ptr<std::ostream> outputStream;
      if (file != "-")
	{
	  boost::shared_ptr<std::ofstream> os (new std::ofstream ());
	  os->open (file.c_str (), std::ios_base::out | std::ios_base::trunc);

	  if (!os->is_open ())
	    {
	      NS_LOG_ERROR ("File " << file << " cannot be opened for writing. Tracing disabled");
	      return;
	    }

	  outputStream = os;
	}
      else
	{
	  outputStream = boost::shared_ptr<std::ostream> (&std::cout, NullDeleter<std::ostream>);
	}

      Ptr<FibTracer> trace = Install (node, outputStream);
      tracers.push_back (trace);

      if (tracers.size () > 0)
	{
	  tracers.front ()->PrintHeader (*outputStream);
	  *outputStream << "\n";
	}

      g_tracers.push_back (boost::make_tuple (outputStream, tracers));
    }

    Ptr<FibTracer>
    FibTracer::Install (Ptr<Node> node,
			boost::shared_ptr<std::ostream> outputStream)
    {
      NS_LOG_DEBUG ("Node: " << node->GetId ());

      Ptr<FibTracer> trace = Create<FibTracer> (outputStream, node);

      return trace;
    }

    void
    FibTracer::Install (const NodeContainer& nodes,
			const std::string& file)
    {
      std::list<Ptr<FibTracer> > tracers;
      boost::shared_ptr<std::ostream> outputStream;
      if (file != "-")
	{
	  boost::shared_ptr<std::ofstream> os (new std::ofstream ());
	  os->open (file.c_str (), std::ios_base::out | std::ios_base::trunc);

	  if (!os->is_open ())
	    {
	      NS_LOG_ERROR ("File " << file << " cannot be opened for writing. Tracing disabled");
	      return;
	    }

	  outputStream = os;
	}
      else
	{
	  outputStream = boost::shared_ptr<std::ostream> (&std::cout, NullDeleter<std::ostream>);
	}

      for (NodeContainer::Iterator node = nodes.Begin ();
	  node != nodes.End ();
	  node++)
	{
	  Ptr<FibTracer> trace = Install (*node, outputStream);
	  tracers.push_back (trace);
	}

      if (tracers.size () > 0)
	{
	  tracers.front ()->PrintHeader (*outputStream);
	  *outputStream << "\n";
	}

      g_tracers.push_back (boost::make_tuple (outputStream, tracers));
    }

    void
    FibTracer::InstallAll (const std::string& file)
    {
      using namespace boost;
      using namespace std;

      std::list<Ptr<FibTracer> > tracers;
      boost::shared_ptr<std::ostream> outputStream;
      if (file != "-")
	{
	  boost::shared_ptr<std::ofstream> os (new std::ofstream ());
	  os->open (file.c_str (), std::ios_base::out | std::ios_base::trunc);

	  if (!os->is_open ())
	    {
	      NS_LOG_ERROR ("File " << file << " cannot be opened for writing. Tracing disabled");
	      return;
	    }

	  outputStream = os;
	}
      else
	{
	  outputStream = boost::shared_ptr<std::ostream> (&std::cout, NullDeleter<std::ostream>);
	}

      for (NodeList::Iterator node = NodeList::Begin ();
	  node != NodeList::End ();
	  node++)
	{
	  Ptr<FibTracer> trace = Install (*node, outputStream);
	  tracers.push_back (trace);
	}

      if (tracers.size () > 0)
	{
	  // *m_l3RateTrace << "# "; // not necessary for R's read.table
	  tracers.front ()->PrintHeader (*outputStream);
	  *outputStream << "\n";
	}

      g_tracers.push_back (boost::make_tuple (outputStream, tracers));
    }

    void
    FibTracer::Destroy ()
    {
      g_tracers.clear ();
    }

    void
    FibTracer::PrintHeader (std::ostream& os) const
    {
      os << "Time" << "\t"
	  << "Node" << "\t"
	  << "ICN" << "\t"
	  << "Type" << "\t"
	  << "Entries" << ""
	  ;
    }

    void
    FibTracer::Connect ()
    {
      Ptr<Fib> fib = m_nodePtr->GetObject<Fib> ();

      NS_ASSERT_MSG((fib != 0), "FIB not found on node, check code!");

      fib->TraceConnectWithoutContext("Name3NEntries", MakeCallback (&FibTracer::Fib3NPartUpdate, this));
      fib->TraceConnectWithoutContext("NamePoAEntries", MakeCallback (&FibTracer::FibPoAPartUpdate, this));
    }

    void
    FibTracer::Fib3NPartUpdate (Ptr<const icn::Name> name, size_t size)
    {
      *m_os << Simulator::Now ().ToDouble (Time::S) << "\t"
	  << m_node << "\t"
	  << *name << "\t"
	  << "3N" << "\t"
	  << size << "\n"
	  ;
    }

    void
    FibTracer::FibPoAPartUpdate (Ptr<const icn::Name> name, size_t size)
    {
      *m_os << Simulator::Now ().ToDouble (Time::S) << "\t"
	  << m_node << "\t"
	  << *name << "\t"
	  << "PoA" << "\t"
	  << size << "\n"
	  ;
    }
  } /* namespace nnn */
} /* namespace ns3 */
