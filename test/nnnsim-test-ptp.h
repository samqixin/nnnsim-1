/* -*- Mode:C++; c-file-style:"gnu"; -*- */
/*
 * nnn-test-p2p.cc
 *  Basic tests for 3N on P2P
 *
 * Copyright (c) 2020 Jairo Eduardo Lopez
 * Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 */

// ns3 modules
#include "ns3/test.h"
#include "ns3/config.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// nnnSIM modules 
#include "ns3/nnnsim-module.h"

using namespace ns3;
using namespace boost;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("PointToPoint3NTest");

namespace ns3
{
  namespace nnn
  {
    class PointToPoint3NTest : public TestCase
    {
      public:
        PointToPoint3NTest ();

        virtual void
        DoRun (void);

      private:
        void SendEnroll (Ptr<Node> node, Time time);
    };

    PointToPoint3NTest::PointToPoint3NTest ()
      : TestCase ("PointToPoint3N")
    {
    }

    void
    PointToPoint3NTest::DoRun (void)
    {
      int    num_nodes = 5;
      double endTime   = 60;
      std::string forwardType = "ns3::nnn::ICN3NSmartFlooding";

      NS_LOG_INFO ("------Creating nodes------");

      NodeContainer nodes;
      nodes.Create (num_nodes);

      NS_LOG_INFO ("------Placing nodes-------");
      Ptr<ListPositionAllocator> initialCenter = CreateObject<ListPositionAllocator> ();

      // Node positions in order
      initialCenter->Add (Vector (0.0, 0.0, 0.0));
      initialCenter->Add (Vector (0.0, 5.0, 0.0));
      initialCenter->Add (Vector (-5.0, 10.0, 0.0));
      initialCenter->Add (Vector (5.0, 10.0, 0.0));
      initialCenter->Add (Vector (-10.0, 15.0, 0.0));

      MobilityHelper centralStations;
      centralStations.SetPositionAllocator (initialCenter);
      centralStations.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
      centralStations.Install (nodes);

      // Connect nodes
      // PtP connections are 100Mbps with 5ms delay
      NS_LOG_INFO("------Connecting nodes------");

      NetDeviceContainer ptpDevices;

      PointToPointHelper p2p_100mbps5ms;
      p2p_100mbps5ms.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
      p2p_100mbps5ms.SetChannelAttribute ("Delay", StringValue ("5ms"));

      PointToPointHelper p2p_100mbps10ms;
      p2p_100mbps10ms.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
      p2p_100mbps10ms.SetChannelAttribute ("Delay", StringValue ("10ms"));

      PointToPointHelper p2p_100mbps20ms;
      p2p_100mbps20ms.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
      p2p_100mbps20ms.SetChannelAttribute ("Delay", StringValue ("20ms"));

      NS_LOG_INFO ("Connecting Node 0 to 1 via 5ms PtP");
      ptpDevices.Add (p2p_100mbps10ms.Install (nodes.Get (0), nodes.Get (1)));
      NS_LOG_INFO ("Connecting Node 0 to 4 via 20ms PtP");
      ptpDevices.Add (p2p_100mbps20ms.Install (nodes.Get (0), nodes.Get (4)));

      NS_LOG_INFO ("Connecting Node 1 to 2 via 5ms PtP");
      ptpDevices.Add (p2p_100mbps5ms.Install (nodes.Get (1), nodes.Get (2)));
      NS_LOG_INFO ("Connecting Node 1 to 3 via 20ms PtP");
      ptpDevices.Add (p2p_100mbps20ms.Install (nodes.Get (1), nodes.Get (3)));

      NS_LOG_INFO ("Connecting Node 2 to 3 via 5ms PtP");
      ptpDevices.Add (p2p_100mbps5ms.Install (nodes.Get (2), nodes.Get (3)));

      NS_LOG_INFO ("Connecting Node 2 to 4 via 5ms PtP");
      ptpDevices.Add (p2p_100mbps5ms.Install (nodes.Get (2), nodes.Get (4)));

      NS_LOG_INFO ("------ Installing 3N stack ------");
      // Stack for a Node that is given a node name
      nnn::NNNStackHelper usr3NStack;
      // Set the Forwarding Strategy
      usr3NStack.SetForwardingStrategy (forwardType,
              "Use3NName", "true",
              "3NRetransmitTime", "11ms");

      std::string cs ("1000000");

      // Set the Content Store for the primary stack, Normal LRU ContentStore
      usr3NStack.SetContentStore ("ns3::nnn::cs::Freshness::Lru", "MaxSize", cs);
      // Set the FIB default routes
      usr3NStack.SetDefaultRoutes (true);
      usr3NStack.SetPoASharing (true);
      // Install the stack
      usr3NStack.Install (nodes);

      // Create the initial 3N name
      Ptr<nnn::NNNAddress> firstName = Create <nnn::NNNAddress> ("a");
      // Get the ForwardingStrategy object from the node
      Ptr<nnn::ForwardingStrategy> CentralFW = nodes.Get (0)->GetObject<nnn::ForwardingStrategy> ();
      // Give a 3N name for the first AP - ensure it is longer than the actual simulation
      CentralFW->SetNode3NName (firstName, Seconds (endTime + 35), true);

      SendEnroll(nodes.Get(1), Seconds(0));

      Simulator::Stop (Seconds (endTime));
      Simulator::Run ();
      Simulator::Destroy ();
    }

    void
    PointToPoint3NTest::SendEnroll (Ptr<Node> node, Time time)
    {
      Ptr<nnn::ForwardingStrategy> fw = node->GetObject<nnn::ForwardingStrategy> ();
      Simulator::Schedule(time, &nnn::ForwardingStrategy::Enroll, fw);
    }
  }
}
