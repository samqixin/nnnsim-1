/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * nnnsim-test-suite.cc
 *    Main file to test the nnnsim module
 *
 * Copyright (c) 2020 Jairo Eduardo Lopez
 * Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 */

// Include a header file from your module to test.
#include "ns3/nnnsim-module.h"

// An essential include is test.h
#include "ns3/test.h"

// Simple PointToPoint tests
#include "nnnsim-test-ptp.h"

// Do not put your test classes in namespace ns3.  You may find it useful
// to use the using directive to access the ns3 namespace directly
using namespace ns3;

class NnnsimTestSuite : public TestSuite
{
public:
  NnnsimTestSuite ();
};

NnnsimTestSuite::NnnsimTestSuite ()
  : TestSuite ("nnnsim", UNIT)
{
  // TestDuration for TestCase can be QUICK, EXTENSIVE or TAKES_FOREVER
  AddTestCase (new nnn::PointToPoint3NTest, TestCase::QUICK);
}

// Do not forget to allocate an instance of this TestSuite
static NnnsimTestSuite nnnsimTestSuite;

