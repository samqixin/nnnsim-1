#!/bin/bash

# icc-run.sh
#
# Script for running 3N ICC scenario
# These scenarios alternates the position of the consumer and the producer
# Runs PROC number of scenarios at the same time.
#
# Copyright (c) 2016 Jairo Eduardo Lopez
#
# icc-run.sh is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icc-run.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#                                                                             
# You should have received a copy of the GNU Affero Public License
# along with icc-run.sh.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
#

SPEEDS="1.40 2.80 4.20 5.60 7.00 8.40 9.80 11.20"
NSscenario=nnn-icc-mobility

MAIN=.
OUTDIR=${MAIN}/results/graphs
PROC=3
EXE=0
###############################################################################

if [ ! -d $OUTDIR ]; then
    echo "Error: Please create ${OUTDIR}!"
    exit 1
fi

speedstr=""
for i in $SPEEDS
do
    speed=${i//\./-}

    # Run 3N Consumer
    (./waf --run "$NSscenario --mobile --speed=$i --trace") &
    echo "Running 3N Mobile Consumer $i"

    EXE=`expr $EXE + 1`
    if (( $EXE % $PROC == 0 )); then
        echo "Already have $PROC processes running, waiting"
        wait
    fi
    
    sleep 1

    # Run 3N Producer
    (./waf --run "$NSscenario --mobile --speed=$i --producer --trace") &
    echo "Running 3N Mobile Producer $i"

    EXE=`expr $EXE + 1`
    if (( $EXE % $PROC == 0 )); then
        echo "Already have $PROC processes running, waiting"
        wait
    fi

    sleep 1

    # Run ICN Smart Flooding Consumer
    (./waf --run "$NSscenario --speed=$i --trace") &
    echo "Running ICN Mobile Consumer $i"

    EXE=`expr $EXE + 1`
    if (( $EXE % $PROC == 0 )); then
        echo "Already have $PROC processes running, waiting"
        wait
    fi

    sleep 1

    # Run ICN Smart Flooding Producer
    (./waf --run "$NSscenario --speed=$i --producer --trace") &
    echo "Running ICN Mobile Producer $i"

    EXE=`expr $EXE + 1`
    if (( $EXE % $PROC == 0 )); then
        echo "Already have $PROC processes running, waiting"
        wait
    fi

    sleep 1
done

wait
